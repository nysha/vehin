<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');



// Here is where you can register API routes for your application.
Route::group([
        'prefix' => 'Api'
],function() {


     // Test module
    Route::group([
        'prefix' => 'island',
        'namespace' => '\Api\Modules\Islands'
    ], function() {


        Route::get('/islands', 'IslandController@all');


    });

     Route::group([
        'prefix' => 'zone',
        'namespace' => '\Api\Modules\Zone'
    ], function() {

        Route::get('/all', 'ZoneController@all');
        Route::post('/create', 'ZoneController@store');
        Route::post('/delete', 'ZoneController@delete');

    });


    Route::group([
        'prefix' => 'BusRoute',
        'namespace' => '\Api\Modules\BusRoute'
    ], function() {

        Route::get('/all', 'BusRouteController@all');
        Route::post('/create', 'BusRouteController@store');
        Route::post('/delete', 'BusRouteController@delete');

    });

    Route::group([
        'prefix' => 'Stop',
        'namespace' => '\Api\Modules\Stop'
    ], function() {

        Route::get('/all', 'StopController@all');
        Route::post('/create', 'StopController@store');
        Route::post('/delete', 'StopController@delete');

    });


 });