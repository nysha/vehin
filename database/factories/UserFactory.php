<?php

use Faker\Generator as Faker;
use Api\Modules\User\Models\User;
use Api\Modules\User\Definitions\UserDefinitions;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
    	'username' => 'user' . $faker->randomNumber(5),
        'name' => $faker->name,
        'type' => array_random(['admin', 'super_admin']),
        'password' => \Hash::make(UserDefinitions::DEFAULT_PASSWORD),
    ];
});
