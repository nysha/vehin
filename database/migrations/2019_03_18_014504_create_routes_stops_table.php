<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Api\Core\Base\BaseMigration;

class CreateRoutesStopsTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes_stops', function (Blueprint $table) {
            $table->increments('id');
            $this->foreign($table, 'routes');
            $this->foreign($table, 'stops');
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes_stops');
    }
}
