<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("stops", function (Blueprint $table) {
            $table->dropColumn(['stop_code']);
            $table->string('mtcc_st_code')->after('id')->nullable();
            $table->string('stop_name')->after('mtcc_st_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("stops", function (Blueprint $table) {
            $table->string('stop_code')->after('id');
            $table->dropColumn(['mtcc_st_code', 'stop_name']);
        });
    }
}
