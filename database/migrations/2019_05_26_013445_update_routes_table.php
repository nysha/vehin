<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Api\Core\Base\BaseMigration;

class UpdateRoutesTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->string('mtcc_rt_id')->nullable();
            $table->float('fare_adult', 5,2)->default(0.00);
            $table->float('fare_underage', 5,2)->default(0.00);
            $table->float('fare_infant', 5,2)->default(0.00);
            $table->string('accessories_allowed')->nullale();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn(['mtcc_rt_id', 'fare_adult', 'fare_underage', 'fare_infant', 'accessories_allowed']);
        });
    }
}
