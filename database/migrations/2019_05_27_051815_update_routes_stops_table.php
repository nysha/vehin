<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Api\Core\Base\BaseMigration;

class UpdateRoutesStopsTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes_stops', function(Blueprint $table) {
            $table->string('d_stop_id')->after('stop_id')->nullable();
            $table->dateTime('o_dep_time')->after('d_stop_id')->nullable();
            $table->dateTime('d_arr_time')->after('o_dep_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes_stops', function(Blueprint $table) {
            $table->dropColumn(['d_stop_id', 'o_dep_time', 'd_arr_time']);
        });
    }
}
