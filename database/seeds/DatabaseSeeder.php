<?php

use Api\Modules\Auth\Definitions\PermissionDefinitions;
use Api\Modules\Auth\Models\Permission;
use Api\Modules\Auth\Services\PermissionService;
use Api\Modules\Room\Models\Room;
use Api\Modules\User\Definitions\UserDefinitions;
use Api\Modules\User\Models\User;
use Illuminate\Database\Seeder;
use Api\Modules\BusRoute\Models\Route;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * @var PermissionService
     */
    private $permissionService;

    /**
     * DatabaseSeeder constructor.
     *
     * @param PermissionService $permissionService
     */
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // create super admin
//        $superAdmin = User::create([
//            'username' => '000',
//            'name' => UserDefinitions::SUPER_ADMIN_NAME,
//            'password' => UserDefinitions::SUPER_ADMIN_PASSWORD,
//            'type' => UserDefinitions::SUPER_ADMIN_TYPE
//        ]);
//        $this->command->info("Super admin created...");
//        $this->command->line('====================================');
//
//        $users = factory(User::class, 20)->create();
//
//
//        $permissions = PermissionDefinitions::get();
//        foreach ($permissions as $permission) {
//            Permission::create($permission);
//        }
//        $this->command->info("Permissions Table seeded...");
//        $this->command->line('====================================');
//
//        $permissionService = app(PermissionService::class);
//        $userTypePermissions = PermissionDefinitions::getUserTypePermissions();
//        $users->each(function (User $user) use ($userTypePermissions) {
//            $this->attachUserPermissions($user, $userTypePermissions);
//        });
//        $this->command->info("Permissions attached to seeded users...");
//        $this->command->line('====================================');
//
//        $this->attachUserPermissions($superAdmin, $userTypePermissions);
//        $this->command->info("Permissions attached to super admin user...");
//        $this->command->line('====================================');

        //seed routes
        $this->command->info("Adding Routes ... ");
        $this->command->line('====================================');

        $dt = new Carbon();
        $data = [
            [
                "route_name" => "Hulhumale to Velana International Airport (VIA)",
                "desc"  => "Hulhumale to Velana international \n\nMain transport routes and between Hulhumale’ Velana International Airport (VIA).",
                "mtcc_rt_id" => "100",
                "fare_adult" => 20.00,
                "fare_underage" => 20.00,
                "fare_infant" => 20.00,
                "accessories_allowed" => "Light Luggage",
                "created_at" => $dt,
                "updated_at" => $dt
            ],
            [
                "route_name" => "Hulhumale' Neighborhood Bus",
                "desc"  => "Bus Service between the Neighborhoods of Hulhumale' and Meyna Gimatha (Hulhumale Ferry Terminal)",
                "mtcc_rt_id" => "105",
                "fare_adult" => 3.00,
                "fare_underage" => 3.00,
                "fare_infant" => 3.00,
                "accessories_allowed" => "Light Luggage",
                "created_at" => $dt,
                "updated_at" => $dt
            ],
        ];

        \Illuminate\Support\Facades\DB::table('routes')->insert($data);

        $this->command->info("Routes Added ... ");
        $this->command->line('====================================');

    }

    /**
     * Attach permission to its users
     *
     * @param User  $user
     * @param array $userTypePermissions
     */
    private function attachUserPermissions(User $user, $userTypePermissions)
    {
        $permissionSlugs = $userTypePermissions[$user->type];
        $permissions = $this->permissionService->getBySlugsOrFail($permissionSlugs);
        $user->permissions()->sync($permissions);
    }
}
