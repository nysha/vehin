<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportMtccData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtcc:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to run all MTCC Import Commands to import routes, stops and route_stops.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Import Routes');
        $this->call('mtcc:import-routes');
        $this->info('Routes Imported');
        $this->info('=========================================');

        $this->info('Import Stops');
        $this->call('mtcc:import-stops');
        $this->info('Stops Imported');
        $this->info('=========================================');

        $this->info('Import Route Stops');
        $this->call('mtcc:import-routes-stops');
        $this->info('Route Stops Imported');
        $this->info('=========================================');
    }
}
