<?php

namespace App\Console\Commands;

use Api\Modules\Stop\Models\Stop;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use Carbon\Carbon;


class ImportStops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtcc:import-stops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command can be used to import the Mtcc Stop details from a CSV file. It will only import those not in DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // read file stores at data/
        $this->info('Reading CSV File');
        $csv = Reader::createFromPath(storage_path() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mtcc_stops.csv');
        $csv->setHeaderOffset(0);

        $this->info('Writing records to DB');
        $data = $csv->getRecords();
        foreach($data as $stop) {
            //set Created_at and Updated_at
            $stop['created_at'] = $stop['updated_at'] = new Carbon();
            $mtccStCode = array_pull($stop, 'mtcc_st_code');
            Stop::updateOrInsert(['mtcc_st_code' => $mtccStCode], $stop);
        }
        $this->info('Write Completed');
    }
}
