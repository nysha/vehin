<?php

namespace App\Console\Commands;

use Api\Modules\BusRoute\Models\Route;
use Api\Modules\Stop\Models\Stop;
use Illuminate\Console\Command;
use League\Csv\Reader;

class ImportRouteStops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtcc:import-routes-stops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to insert route stops';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // read file stores at data/
        $this->info('Reading CSV File');
        $csv = Reader::createFromPath(storage_path() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mtcc_route_stops.csv');
        $csv->setHeaderOffset(0);

        $this->info('Writing records to DB');
        $data = $csv->getRecords();
        foreach($data as $rs) {
            $route  = Route::where('mtcc_rt_id', $rs['rt_id'])->first();
            $origin = Stop::where('mtcc_st_code', $rs['osp_id'])->first()->id;
            $destination = Stop::where('mtcc_st_code', $rs['dsp_id'])->first()->id;
            $input = [
//                'route_id' => $route->id,
//                'stop_id' => $origin,
                'd_stop_id' => $destination,
                'o_dep_time' => $rs['rtt_departure_datetime'],
                'd_arr_time' => $rs['rtt_arrival_datetime'],
                'order' => $rs['rs_seq']
            ];
            $route->stops()->attach($origin, $input);
        }
        $this->info('Write Completed');
    }
}
