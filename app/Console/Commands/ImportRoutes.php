<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use Carbon\Carbon;
use Api\Modules\BusRoute\Models\Route;

class ImportRoutes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mtcc:import-routes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command will be used to import routes in the MTCC database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // read file stores at data/
        $this->info('Reading CSV File');
        $csv = Reader::createFromPath(storage_path() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mtcc_routes.csv');
        $csv->setHeaderOffset(0);

        $this->info('Writing records to DB');
        $data = $csv->getRecords();
        foreach($data as $route) {
            // insert new
            $input = [
                'route_name' => $route['name'],
                'desc' => $route['description'],
                'fare_adult' => $route['fare_adult'],
                'fare_underage' => $route['fare_underage'],
                'fare_infant' => $route['fare_infant'],
                'accessories_allowed' => $route['accessories_allowed']
            ];

            $input['created_at'] = $input['updated_at'] = new Carbon();
            Route::updateOrInsert(['mtcc_rt_id' => $route['rt_id']], $input);
        }
        $this->info('Write Completed');
    }
}
