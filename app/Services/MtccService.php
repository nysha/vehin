<?php

namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class MtccService
 *
 * @package \App\Services
 */
class MtccService
{
    protected $apiPrefix;
    protected $loginEmail;
    protected $loginPassword;

    public function __construct()
    {
        $this->setDefaults();
    }

    public function setDefaults() {
        $this->apiPrefix = config('mtcc.api_prefix');
        $this->loginEmail = config('mtcc.email');
        $this->loginPassword = config('mtcc.password');
    }

    public function login() {
        $url = $this->apiPrefix . '/login';
        $client = new Client(['base_uri' => $this->apiPrefix, 'headers' => ['Content-Type' => 'application/json']]);
        $response = $client->post( '/login', ['json' => ['email' => $this->loginEmail, 'password' => $this->loginPassword]]);
        $body = $response->getBody();
// Implicitly cast the body to a string and echo it
        echo $body;
// Explicitly cast the body to a string
        $stringBody = (string) $body;
// Read 10 bytes from the body
        $tenBytes = $body->read(10);
// Read the remaining contents of the body as a string
        $remainingBytes = $body->getContents();
        dd($remainingBytes);
    }

    public function validateJwt() {

    }
}
