<?php
return [
    'api_prefix' => env('MTCC_API_PREFIX', ''),
    'email' => env('MTCC_EMAIL', ''),
    'password' => env('MTCC_PASSWORD', '')
];

