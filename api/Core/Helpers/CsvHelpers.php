<?php

namespace Api\Core\Helpers;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Response;
use League\Csv\Writer;

class CsvHelpers
{
    /**
     * Convert data to csv
     *
     * @param $data
     *
     * @return string csv response
     */
    public static function dataToCsv($data)
    {
        if ($data instanceof Arrayable) {
            $data = $data->toArray();
        }

        if (! is_array($data)) {
            $data = (array)$data;
        }

        foreach ($data as $key => $datum) {
            $data[$key] = (array)$datum;
        }

        if (count($data) === 0) {
            return '';
        }

        $out = [];

        $csvHeader = [];
        $keys      = Helpers::getArrayKeysRecursive(head($data));

        foreach ($keys as $key) {
            $csvHeader[] = title_case($key);
        }

        foreach ($data as $datum) {
            $obj = [];
            foreach ($keys as $key) {
                $value = array_get($datum, $key);
                if (is_array($value)) {
                    $obj[] = '<Array>';
                } else {
                    $obj[] = $value;
                }
            }

            $out[] = $obj;
        }

        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne($csvHeader);
        $csv->insertAll($out);

        return $csv->getContent();
    }

    /**
     * Respond CSV as an http response
     *
     * @param $data
     * @param $filename
     *
     * @return Response
     */
    public static function respondCsv($data, $filename)
    {
        $csv = static::dataToCsv($data);

        $headers = [
            'Content-Type'        => 'text/csv',
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        ];

        return \Response::make($csv, 200, $headers);
    }
}
