<?php

namespace Api\Core\Helpers;

use Api\Core\Definitions\AppConstants;
use Api\Exceptions\ClientException;
use Api\Exceptions\InvariantViolationException;
use Api\Exceptions\ServerException;
use Api\Modules\Auditor\Models\AuditLog;
use Api\Modules\User\Models\User;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection as DbCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Request;


class Helpers
{
    /**
     * Fetch a value from env which is required
     *
     * @param $key
     * @param $error
     *
     * @return mixed
     */
    public static function configRequired($key, $error)
    {
        $value = config($key);
        if (empty($value)) {
            throw new ServerException("Missing config variable: $error", [
                'key'   => $key,
                'value' => $value,
            ]);
        }

        return $value;
    }

    /**
     * Disable audit logging
     */
    public static function disableAuditLogging()
    {
        app()->bind(AuditLog::DISABLE_AUDITING, '_');
    }

    /**
     * Tag current request as a web request
     */
    public static function tagWebRequest()
    {
        app()->bind(AppConstants::IOC_WEB_REQUEST, '_');
    }

    /**
     * Does the request expects html
     *
     * @return bool
     */
    public static function expectsHtml()
    {
        return app()->has(AppConstants::IOC_WEB_REQUEST) || !request()->acceptsJson();
    }

    /**
     * Get the user timezone or default
     *
     * @param string $default
     *
     * @return string
     */
    public static function tz($default = 'UTC')
    {
        /** @var User $user */
        $user = \Auth::user();
        if ($user !== null) {
            return $user->company->tz();
        }

        return $default;
    }

    /**
     * From an array remove all keys with the given prefix recursively
     *
     * @param $prefix
     * @param $obj
     *
     * @return array
     */
    public static function removePrefixedRecursive($prefix, $obj)
    {
        $out = [];
        foreach ($obj as $key => $value) {
            if (starts_with($key, $prefix)) {
                continue;
            }

            if (is_array($value)) {
                $value = static::removePrefixedRecursive($prefix, $value);
            }

            $out[$key] = $value;
        }

        return $out;
    }

    /**
     * Are the two doubles considered equal
     *
     * @param       $a
     * @param       $b
     * @param float $epsilon
     *
     * @return bool
     */
    public static function isDoubleEqual($a, $b, $epsilon = AppConstants::EPSILON)
    {
        return abs($a - $b) < $epsilon;
    }

    /**
     * Format currency
     *
     * @param        $amount
     * @param string $currency
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeparator
     *
     * @return string
     */
    public static function formatMoney(
        $amount,
        $currency = '',
        $decimals = 2,
        $decimalPoint = '.',
        $thousandSeparator = ','
    ) {
        return (! empty($currency) ? "$currency " : '') . number_format($amount, $decimals, $decimalPoint, $thousandSeparator);
    }

    /**
     * Get all duplicate elements from a simple primitive element array
     *
     * @param array $array
     *
     * @return array
     */
    public static function arrayDuplicates(array $array)
    {
        $out = [];
        foreach ($array as $obj) {
            if (! isset($out[$obj])) {
                $out[$obj] = 0;
            }

            $out[$obj]++;
        }

        $duplicates = [];
        foreach ($out as $key => $count) {
            if ($count > 1) {
                $duplicates[] = $key;
            }
        }

        return array_unique($duplicates);
    }

    /**
     * Get array keys of an object recursively
     *
     * @param array $array
     *
     * @return array
     */
    public static function getArrayKeysRecursive(array $array)
    {
        $out = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                foreach (static::getArrayKeysRecursive($value) as $key2) {
                    $out[] = $key . '.' . $key2;
                }
            } else {
                $out[] = $key;
            }
        }

        return $out;
    }

    /**
     * Check if relation is loaded on a loadable object like a DB collection or Model.
     *
     * Default model implementation does not work with nested relations. This does
     *
     * @param                            $relation
     * @param Model|DbCollection|Model[] $obj
     *
     * @return bool
     * @throws InvariantViolationException
     */
    public static function isRelationLoaded($relation, $obj)
    {
        // relation is 'loaded' if the object is non existent. No point trying to load anyway
        if ($obj == null || empty($obj)) {
            return true;
        }

        // ensure type is correct
        if (! ($obj instanceof Model || $obj instanceof DbCollection)) {
            throw new InvariantViolationException("Object of type " . get_class($obj) . " does not support relation loading");
        }

        $parts = explode('.', $relation);
        if (count($parts) !== 1) {
            $first = $parts[0];

            // get without first part
            array_shift($parts);
            $rest = implode(".", $parts);
        } else {
            $first = $parts[0];
            $rest  = null;
        }

        if ($obj instanceof Model) {

            // ensure relation is loaded on object
            if (! $obj->relationLoaded($first)) {
                return false;
            }
        } else {

            // ensure relation is loaded on all objects
            foreach ($obj as $o) {
                if (! $o->relationLoaded($first)) {
                    return false;
                }
            }
        }

        // we have sub relations. Check them
        if ($rest != null) {
            if ($obj instanceof Model) {

                // ensure relation is loaded in sub relation
                return static::isRelationLoaded($rest, $obj->$first);
            } else {

                // ensure relation is loaded in sub relations of all objects
                foreach ($obj as $o) {
                    if (! static::isRelationLoaded($rest, $o->$first)) {
                        return false;
                    }
                }
            }
        }

        // relation is loaded
        return true;
    }

    /**
     * Checks if the object given is a sequential array
     *
     * @param mixed $obj possible array object
     *
     * @return bool is it a sequential array
     */
    public static function isSequentialArray($obj)
    {
        if ($obj instanceof Collection) {
            return true;
        }

        if (is_array($obj)) {
            $i = 0;
            foreach ($obj as $key => $value) {
                if ($key !== $i) {
                    return false;
                }

                $i++;
            }

            // all indexes are integers starting from 0
            return true;
        }

        return false;
    }

    /**
     * Checks weather the values of two associative arrays are equal
     *
     * @param $a array one
     * @param $b array two
     *
     * @return boolean are they equal
     */
    public static function areAssocArraysEqual($a, $b)
    {
        // ensure both have same number of keys
        if (count(array_keys($a)) != count((array_keys($b)))) {
            return false;
        }

        // ensure all values match
        foreach ($a as $key => $value) {
            if (! isset($b[$key])) {
                return false;
            }

            if ($value != $b[$key]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Try to get a key from an object. Returns the default if not possible
     *
     * @param $obj     array associative array object
     * @param $key     string default value
     * @param $default mixed default value
     *
     * @return mixed
     */
    public static function getValue($obj, $key, $default = '')
    {
        if (empty($obj) || ! is_array($obj)) {
            return $default;
        }

        try {
            return $obj[$key];
        } catch (Exception $e) {
            return $default;
        }
    }

    /**
     * Given a path string traverse an array object and return the value
     *
     * Supports syntax:
     * - a.b.c
     * - [0]
     * - a.b[0]
     * - a.b[2].c
     *
     * @param array  $obj     object
     * @param string $path    path
     * @param mixed  $default default value
     *
     * @return mixed
     */
    public static function pathGet($obj, $path, $default = null)
    {
        $paths = explode('.', $path);

        $object = $obj;
        foreach ($paths as $section) {
            if (empty($object)) {
                return $default;
            }

            if (strpos($section, '[') !== false) {
                // array access
                $index = (int)explode(']', explode('[', $section)[1])[0];
                $key   = explode('[', $section)[0];

                if (is_array($object)) {
                    $object = isset($object[$key]) ? $object[$key] : null;
                } else if (is_object($object)) {
                    $object = isset($object->$key) ? $object->$key : null;
                } else {
                    throw new InvariantViolationException("Not an array or object passed");
                }

                if (isset($object[$index])) {
                    $object = $object[$index];
                } else {
                    $object = null;
                }
            } else {
                if (is_array($object)) {
                    $object = isset($object[$section]) ? $object[$section] : null;
                } else if (is_object($object)) {
                    $object = isset($object->$section) ? $object->$section : null;
                } else {
                    throw new InvariantViolationException("Not an array or object passed");
                }
            }
        }

        if ($object !== null) {
            return $object;
        }

        return $default;
    }

    /**
     * Set a value on an object given path notation
     *
     * @param $obj
     * @param $path
     * @param $value
     */
    public static function pathSet(&$obj, $path, $value)
    {
        $subPaths = explode('.', $path);

        // create the object up to the point
        foreach (array_slice($subPaths, 0, -1) as $subPath) {

            // array access
            if (strpos($subPath, '[') !== false) {
                $index = (int)explode(']', explode('[', $subPath)[1])[0];
                $key   = explode('[', $subPath)[0];

                if (! isset($obj[$key])) {
                    $obj[$key] = [];
                }

                if (! isset($obj[$key][$index])) {
                    $obj[$key][$index] = [];
                }

                $obj = &$obj[$key][$index];
            } else {

                if (! isset($obj[$subPath])) {
                    $obj[$subPath] = [];
                }

                $obj = &$obj[$subPath];
            }
        }

        $lastSegment = end($subPaths);

        // if the last value ends with [] we will append to an array
        // instead of replacing
        if (ends_with($lastSegment, '[]')) {
            $lastSegment = substr($lastSegment, 0, -2);

            if (! isset($obj[$lastSegment])) {
                $obj[$lastSegment] = [];
            }

            $obj[$lastSegment][] = $value;
        } else {
            $obj[$lastSegment] = $value;
        }
    }

    /**
     * Given an object of key => [old, new] generate a human readable string for
     * the diff
     *
     * @param       $obj
     * @param array $fieldMapping map of actual field names to display names
     *
     * @return string
     */
    public static function generateDiffString($obj, $fieldMapping = [])
    {
        $str    = '';
        $prefix = '';
        foreach ($obj as $key => $value) {
            $str .= $prefix;

            if (isset($fieldMapping[$key])) {
                $key = $fieldMapping[$key];
            }

            if (empty($value['old'])) {
                $str .= "$key changed to \"{$value['new']}\" (old: empty)";
            } else {
                $str .= "$key changed to \"{$value['new']}\" (old: \"{$value['old']}\")";
            }

            if ($prefix == '') {
                $prefix = ', ';
            }
        }

        return $str;
    }

    /**
     * Initiate query logging
     *
     * @param bool $force
     */
    public static function initiateQueryLog($force = false)
    {
        if (! $force) {
            $hasSqlParam = Request::has('sql') || env('DEBUG_SQL');
            if (! $hasSqlParam) {
                return;
            }
        }

        DB::listen(function ($query) {
            $obj = [
                'sql'      => $query->sql,
                'bindings' => $query->bindings,
                'time'     => $query->time,
            ];

            \Log::info('[Query]', $obj);
        });
    }

    /**
     * Get morph name for a class
     *
     * @param $class
     *
     * @return false|int|string
     */
    public static function getMorphName($class)
    {
        $morphMap = Relation::morphMap();

        if (! empty($morphMap) && in_array($class, $morphMap)) {
            return array_search($class, $morphMap, true);
        }

        return $class;
    }

    /**
     * Template of file name
     *
     * @param string $template allowed template variables TODAY
     * @param array  $vars
     *
     * @return mixed|string
     */
    public static function generateTemplate($template, $vars = [])
    {
        $map = [
            '{TODAY}' => Carbon::today(Helpers::tz())->format('d-m-Y'),
        ];

        $map = array_merge($map, $vars);

        foreach ($map as $key => $value) {
            $template = str_replace($key, $value, $template);
        }

        return $template;
    }

    /**
     * Create from carbon format with a better error
     *
     * @param string $format
     * @param string $time
     * @param null   $tz
     *
     * @return Carbon
     */
    public static function carbonCreateFromFormat($format, $time, $tz = null)
    {
        try {
            return Carbon::createFromFormat($format, $time, $tz);
        } catch (\InvalidArgumentException $e) {
            throw new ClientException("Date $time is in an invalid format. Expected to be in format $format");
        }
    }

    /**
     * Timestamp boundaries of the day in UTC
     *
     * @param Carbon $date
     *
     * @return array
     */
    public static function boundariesOfUserDayInUtc(Carbon $date)
    {
        $start = Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $date->toDateString() . ' 00:00:00',
            Helpers::tz()
        );

        $start->tz('UTC');

        $end = $start->copy()->addHour(24);

        return [$start, $end];
    }
}
