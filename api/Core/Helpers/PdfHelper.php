<?php

namespace Api\Core\Helpers;


use Api\Core\Definitions\AppConstants;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class PdfHelper
{
    /**
     * Is this a pdf debug request
     *
     * @return bool
     */
    public static function isDebug()
    {
        return request()->has(AppConstants::REQ_PDF_DEBUG);
    }

    /**
     * Return raw data of a pdf as a view
     *
     * @param       $view
     * @param array $data
     * @param array $options
     *
     * @return string raw pdf data
     */
    public static function view($view, $data = [], $options = [])
    {
        if (static::isDebug()) {
            // in debug mode return html
            return view($view, $data)->render();
        }

        $options = array_merge([
            'header-center'    => 'Vehin Portal' . (isset($data['title']) ? " - {$data['title']}" : ''),
            'header-font-size' => 8,
            'footer-font-size' => 8,
            'footer-right'     => 'Page [page] of [toPage]',
            'footer-left'      =>
                'Generated at '
                . Carbon::now(Helpers::tz())->toDateTimeString()
                . (isset($data['generated_by']) ? " by {$data['generated_by']}" : ''),
        ], $options);

        $pdf = app('snappy.pdf.wrapper');
        $pdf->loadView($view, $data);

        foreach ($options as $key => $value) {
            $pdf->setOption($key, $value);
        }

        return $pdf->output();
    }

    /**
     * Respond pdf file
     *
     * @param string $raw
     * @param string $filename
     *
     * @return Response
     */
    public static function respondPdf($raw, $filename)
    {
        if (static::isDebug()) {
            return \Response::make($raw);
        }

        $headers = [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        ];

        return \Response::make($raw, 200, $headers);
    }
}
