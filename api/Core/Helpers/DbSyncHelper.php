<?php

namespace Api\Core\Helpers;

use DB;
use Exception;


class DbSyncHelper
{
    /**
     * Syncs the given changes to database.
     *
     * @param string $table      the table name
     * @param string $primaryKey key to identify duplicates by
     * @param array  $dataSet    array of assoc data arrays
     * @param bool   $dryRun     if a dry run, we will return an array which describes the operations
     *                           but we will not do them
     *
     * @return array array of messages describing operations that can be shown
     *                           to the user
     */
    public static function sync($table, $primaryKey, array $dataSet, $dryRun = false)
    {
        // we need to truncate the whole table
        if (empty($dataSet)) {
            if ($dryRun) {
                return [true, ['Truncating ' . $table . '!']];
            }

            try {
                DB::table($table)->delete();

                return [true, "Truncated ${table}"];
            } catch ( Exception $e ) {
                return [false, 'Error truncating ' . $table . ' ' . $e->getMessage()];
            }
        }

        $keys             = array_keys(array_first($dataSet));
        $currentValuesRaw = DB::table($table)->select()->get();

        // transform current values to identical structured assoc arrays
        $currentValues = [];
        foreach ($currentValuesRaw as $row) {
            $row = get_object_vars($row);

            $obj = [];
            foreach ($keys as $key) {
                $obj[$key] = $row[$key];
            }

            $currentValues[] = $obj;
        }

        // build a map of key to data for easy access
        $newMap = [];
        $oldMap = [];
        foreach ($dataSet as $row) {
            $newMap[$row[$primaryKey]] = $row;
        }

        foreach ($currentValues as $row) {
            $oldMap[$row[$primaryKey]] = $row;
        }

        $updates       = [];
        $updatesDryRun = [];
        $deletes       = [];
        $inserts       = [];

        // check for updates and deletions
        foreach ($currentValues as $row) {
            $rowKey = $row[$primaryKey];

            if (! isset($newMap[$rowKey])) {
                // this value has been deleted
                $deletes[] = $rowKey;
            } else if (! Helpers::areAssocArraysEqual($row, $newMap[$rowKey])) {
                // this value has been changed
                $updates[]       = $newMap[$rowKey];
                $updatesDryRun[] = [
                    'old' => $row,
                    'new' => $newMap[$rowKey],
                ];
            }
        }

        // check for inserts
        foreach ($newMap as $obj) {
            if (! isset($oldMap[$obj[$primaryKey]])) {
                $inserts[] = $obj;
            }
        }

        if (empty($updates) && empty($deletes) && empty($inserts)) {
            return [true, ['No changes. DB is in sync']];
        }

        if ($dryRun) {
            return [
                true,
                [
                    'inserts' => $inserts,
                    'updates' => $updatesDryRun,
                    'deletes' => $deletes,
                ],
            ];
        }

        // perform the db operations
        // sync the db
        try {
            $out = [];
            DB::transaction(function () use ($inserts, $updates, $deletes, $table, $primaryKey, &$out) {
                if (! empty($inserts)) {
                    // handle inserts
                    DB::table($table)->insert($inserts);
                    $out[] = 'Inserted ' . count($inserts) . ' items';
                }

                if (! empty($updates)) {
                    // handle updates
                    foreach ($updates as $row) {
                        DB::table($table)->where($primaryKey, $row[$primaryKey])
                          ->update($row);
                    }

                    $out[] = 'Updated ' . count($updates) . ' items';
                }

                // delete
                if (! empty($deletes)) {
                    DB::table($table)->whereIn($primaryKey, $deletes)->delete();
                    $out[] = 'Deleted ' . count($deletes) . ' items';
                }

                $out[] = 'Success!';
            });

            return [true, $out];
        } catch ( Exception $e ) {
            return [false, ['No changes were made. DB change transaction failed: ' . $e->getMessage()]];
        }
    }
}
