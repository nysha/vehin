<?php

namespace Api\Core\Helpers;

use Azaan\LaravelScene\SceneResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;


class EwSceneResponse extends SceneResponse
{
    /**
     * Make pager data from a length aware paginator
     *
     * @param LengthAwarePaginator $paginatedData
     *
     * @return array
     */
    protected static function makePager(LengthAwarePaginator $paginatedData)
    {
        $pager = [
            'total'    => $paginatedData->total(),
            'pageSize' => $paginatedData->perPage(),
            'current'  => $paginatedData->currentPage(),
            'lastPage' => $paginatedData->lastPage(),
        ];

        return $pager;
    }
}
