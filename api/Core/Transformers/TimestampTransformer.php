<?php

namespace Api\Core\Transformers;

use Azaan\LaravelScene\Contracts\ValueTransformation;
use Carbon\Carbon;


class TimestampTransformer implements ValueTransformation
{
    /**
     * Given the value return the transformed value
     *
     * @param $value
     *
     * @return mixed
     */
    public function transform($value)
    {
        if ($value !== null) {
            if ($value instanceof Carbon) {
                return $value->timestamp;
            }

            if (is_string($value)) {
                return Carbon::parse($value)->timestamp;
            }
        }

        return null;
    }
}
