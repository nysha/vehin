<?php

namespace Api\Core\Requests;

use Api\Core\Base\BaseRequest;
use Api\Core\Definitions\FetchParams;


class QueryableRequest extends BaseRequest
{
    public function rules()
    {
        return [
            // download
            FetchParams::FIELD_DOWNLOAD           => 'boolean',

            // paginate fields
            FetchParams::FIELD_NO_PAGINATE        => 'boolean',
            FetchParams::FIELD_PAGE               => 'integer',
            FetchParams::FIELD_LIMIT              => 'integer',

            // search fields
            FetchParams::FIELD_SEARCH_QUERY       => 'string',
            FetchParams::FIELD_SEARCH_FIELD       => 'string',
            FetchParams::FIELD_SEARCH_TYPE        => 'string',
            FetchParams::FIELD_SEARCH_RANGE_START => 'string',
            FetchParams::FIELD_SEARCH_RANGE_END   => 'string',
            FetchParams::FIELD_SEARCH_STRICT      => 'boolean',

            // order
            FetchParams::FIELD_ORDER_FIELD        => 'string',
        ];
    }
}
