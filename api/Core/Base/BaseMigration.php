<?php

namespace Api\Core\Base;

use DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Schema;


abstract class BaseMigration extends Migration
{
    /**
     * BaseMigration constructor.
     */
    public function __construct()
    {
        // this is needed for mysql < 5.7.7
        // see: https://laravel-news.com/laravel-5-4-key-too-long-error
        Schema::defaultStringLength(191);
    }

    /**
     * For standard rounded off money values
     *
     * @param Blueprint $table
     * @param           $column
     *
     * @return \Illuminate\Support\Fluent
     */
    protected function moneyStandard(Blueprint $table, $column)
    {
        return $table->decimal($column, 9, 2)
            ->default(0);
    }

    /**
     * For high precision money values which require more decimal places
     *
     * @param Blueprint $table
     * @param           $column
     *
     * @return \Illuminate\Support\Fluent
     */
    protected function moneyHighPrecision(Blueprint $table, $column)
    {
        return $table->decimal($column, 13, 6)
            ->default(0);
    }

    /**
     * Nullable foreign key
     *
     * @param Blueprint $table
     * @param           $foreignTable
     * @param null      $keyName
     * @param string    $foreignTableId
     */
    protected function foreignNullable(Blueprint $table, $foreignTable, $keyName = null, $foreignTableId = 'id')
    {
        return $this->foreign($table, $foreignTable, $keyName, $foreignTableId, true);
    }

    /**
     * Create an unsigned int foreign key
     *
     * @param Blueprint $table
     * @param           $foreignTable
     * @param null      $keyName
     * @param string    $foreignTableId
     * @param bool      $nullable
     */
    protected function foreign(
        Blueprint $table,
        $foreignTable,
        $keyName = null,
        $foreignTableId = 'id',
        $nullable = false
    ) {
        if ($keyName === null) {
            $keyName = str_singular($foreignTable) . '_id';
        }

        $column = $table->unsignedInteger($keyName);

        if ($nullable) {
            $column->nullable();
        }

        $table->foreign($keyName)
            ->references($foreignTableId)
            ->on($foreignTable);
    }

    /**
     * Add only created user tracking fields (created_at, created_by)
     *
     * @param Blueprint $table
     */
    protected function addUserCreatedTracking(Blueprint $table)
    {
        $this->addUserTrackingFieldsWithTimestamps($table, false, true, false);
    }

    /**
     * Add timestamps
     *
     * @param Blueprint $table
     */
    protected function timestamps(Blueprint $table)
    {
        $table->dateTime('created_at')
            ->nullable()
            ->default(DB::raw('CURRENT_TIMESTAMP'));

        $table->timestamp('updated_at')
            ->nullable()
            ->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
    }

    /**
     * Add user tracking fields (created_by, updated_by, deleted_by)
     * with timestamps (created_at, deleted_at, updated_at)
     *
     * @param Blueprint $table
     * @param bool      $morphable
     * @param bool      $addDeleted
     * @param bool      $addCreated
     * @param bool      $addUpdated
     */
    protected function addUserTrackingFieldsWithTimestamps(
        Blueprint $table,
        $morphable = false,
        $addDeleted = false,
        $addCreated = true,
        $addUpdated = true
    ) {
        if ($addCreated) {
            $table->dateTime('created_at')
                ->nullable()
                ->default(DB::raw('CURRENT_TIMESTAMP'));

            if($morphable) {
                $table->enum('created_by_entity', ['user', 'client'])->nullable();
            }
            $table->unsignedInteger('created_by')->nullable();
        }

        if ($addUpdated) {
            $table->timestamp('updated_at')
                ->nullable()
                ->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            if($morphable) {
                $table->enum('updated_by_entity', ['user', 'client'])->nullable();
            }
            $table->unsignedInteger('updated_by')->nullable();
        }

        if ($addDeleted) {
            $table->dateTime('deleted_at')->nullable();

            if($morphable) {
                $table->enum('deleted_by_entity', ['user', 'client'])->nullable();
            }
            $table->unsignedInteger('deleted_by')->nullable();
        }
    }
}
