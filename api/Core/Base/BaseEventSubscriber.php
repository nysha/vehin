<?php

namespace Api\Core\Base;

use Illuminate\Contracts\Events\Dispatcher;


abstract class BaseEventSubscriber
{
    /**
     * Get mapping from events to function names
     *
     * @return array
     */
    protected abstract function getEventMappings();

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher $events
     */
    public function subscribe($events)
    {
        $mappings = $this->getEventMappings();

        foreach ($mappings as $event => $fn) {
            $events->listen($event, static::class . '@' . $fn);
        }
    }
}
