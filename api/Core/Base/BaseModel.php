<?php

namespace Api\Core\Base;

use Api\Core\Services\RequestQuery\QueryableModel;
use Api\Core\Services\RequestQuery\QueryableModelInterface;
use Illuminate\Database\Eloquent\Model;


class BaseModel extends \Eloquent implements QueryableModelInterface
{
    use QueryableModel;

    protected $guarded    = [];
    public    $timestamps = true;

    /**
     * Default pagination limit
     *
     * @var int
     */
    protected static $defaultPaginationLimit = 20;

    /**
     * Ordering config
     *
     * @var array
     */
    protected static $orderConfig = [
        'default'   => ['id', 'DESC'],
        'orderings' => [
            [
                'key' => 'id',
            ],
        ],
    ];

    /**
     * Definition of how to perform joins required for searching and ordering
     *
     * @var
     */
    protected static $joinConfig = [];

    /**
     * Fields which can be searched by
     *
     * @var array
     */
    protected static $searchConfig = [
        'searches' => [],
    ];

    /**
     * Array to store memoized data
     *
     * @var array
     */
    public $_memoizeArray = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // clear memoized on model updates
        static::updated(function ($model) {
            $model->_memoizeArray = [];
        });
    }

    /**
     * Memoization helper. Will compute the result only once
     *
     * @param string   $key
     * @param \Closure $callback
     *
     * @return mixed
     */
    protected function memoize($key, \Closure $callback)
    {
        if (isset($this->_memoizeArray[$key])) {
            return $this->_memoizeArray[$key];
        }

        $result                    = $callback();
        $this->_memoizeArray[$key] = $result;

        return $result;
    }

    /**
     * Reload the current model instance with fresh attributes from the database.
     *
     * @return Model
     */
    public function refresh()
    {
        // clear memoized values
        $this->_memoizeArray = [];

        return parent::refresh();
    }

    /**
     * Get fields from db for update and then update them
     *
     * @param array    $fields
     * @param \Closure $callback
     *
     * @return mixed
     */
    public function forUpdate(array $fields, \Closure $callback)
    {
        $fields = $this->newQuery()
            ->select($fields)
            ->where($this->primaryKey, '=', $this->getKey())
            ->lockForUpdate()
            ->firstOrFail();

        return $callback($this, $fields);
    }
}
