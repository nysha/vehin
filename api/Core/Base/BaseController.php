<?php

namespace Api\Core\Base;

use Api\Core\Helpers\EwSceneResponse;
use Api\Core\Helpers\Helpers;
use Api\Core\Services\RequestQuery\QueryableModelInterface;
use Api\Core\Services\RequestQuery\RequestQueryService;
use Api\Core\Traits\CoreHelpers;
use Api\Exceptions\ClientException;
use Azaan\LaravelScene\SceneTransformer;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use League\Csv\Writer;


class BaseController extends Controller
{
    use CoreHelpers;

    /**
     * Respond a json response
     *
     * @param mixed            $data
     * @param SceneTransformer $transformer an optional transformer
     * @param array            $extraFields extra objects to be added to response. The key values
     *                                      passed in are added to the response. If a transformer is
     *                                      required pass it in the format ['key' => [$arr, $transformer]]
     *
     * @return JsonResponse|Response
     */
    protected function respond($data, SceneTransformer $transformer = null, $extraFields = null)
    {
        $extraFields = $this->addSearchConfig($data, $extraFields);

        $shouldDownload = (bool)request()->input(RequestQueryService::FIELD_DOWNLOAD, false);
        if ($shouldDownload) {

            if ($transformer !== null) {
                $transformer->useDownloadStructure = true;
                $data = $transformer->transform($data);
            }

            return $this->downloadToCsv($data);
        }

        return EwSceneResponse::respond($data, $transformer, $extraFields);
    }

    /**
     * Add search config
     *
     * @param $data
     * @param $extraFields
     *
     * @return array|null
     */
    protected function addSearchConfig($data, $extraFields)
    {
        if ($data instanceof Collection || $data instanceof LengthAwarePaginator) {
            $first = $data->first();

            if (!$first instanceof QueryableModelInterface) {
                return $extraFields;
            }

            // queryable interface
            if ($extraFields === null) {
                $extraFields = [];
            }

            $extraFields['search'] = Helpers::removePrefixedRecursive('_', $first::getSearchConfig());
        }

        return $extraFields;
    }

    /**
     * Download array data to CSV
     *
     * @param array $data
     *
     * @return Response
     */
    protected function downloadToCsv($data)
    {
        if ($data instanceof Arrayable) {
            $data = $data->toArray();
        }

        if (!is_array($data)) {
            $data = (array)$data;
        }

        foreach ($data as &$datum) {
            $datum = (array)$datum;
        }

        $filename = 'MTCC-Download-' . Carbon::today(Helpers::tz())->format('d-m-Y') . '.csv';
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        ];

        if (count($data) === 0) {
            return \Response::make('', 200, $headers);
        }

        $out = [];

        // find a better solution.
        // we randomly check keys for 50 items and join them.
        $csvHeader = [];
        $keys = Helpers::getArrayKeysRecursive(head($data));

        foreach ($keys as $key) {
            $csvHeader[] = title_case($key);
        }

        foreach ($data as $datum) {
            $obj = [];
            foreach ($keys as $key) {
                $value = array_get($datum, $key);
                if (is_array($value)) {
                    $obj[] = '<Array>';
                } else {
                    $obj[] = $value;
                }
            }

            $out[] = $obj;
        }

        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne($csvHeader);
        $csv->insertAll($out);

        return \Response::make($csv->getContent(), 200, $headers);
    }

    /**
     * Empty success response
     *
     * @return JsonResponse
     */
    protected function respondOk()
    {
        return new JsonResponse();
    }

    /**
     * Check if user has permission to perform a specific action
     *
     * @param $permission
     *
     * @return bool
     * @throws \Api\Exceptions\ClientException
     */
    protected function checkPermission($permission)
    {
        $user = $this->getLoggedInUser();

        if (!$user->permissions()->where('slug', $permission)->first()) {
            throw new ClientException('The user does not have sufficient permission to perform this task');
        }
        return true;
    }

    /**
     * Check if the model belongs to the logged in user
     *
     * @param Model $model
     *
     * @return bool
     * @throws \Api\Exceptions\ClientException
     */
    protected function ownsResource(Model $model)
    {
        $user = $this->getLoggedInUser();
        if ($model->user_id == $user->id) {
            throw new ClientException('The user is not authorized to access this resource');
        }
        return true;
    }
}
