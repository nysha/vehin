<?php

namespace Api\Core\Definitions;

class FetchParams
{
    // download
    const FIELD_DOWNLOAD = 'fetchDownload';

    // paginate fields
    const FIELD_NO_PAGINATE = 'noPaginate';
    const FIELD_PAGE        = 'page';
    const FIELD_LIMIT       = 'limit';

    // search fields
    const FIELD_SEARCH_QUERY       = 'searchQuery';
    const FIELD_SEARCH_FIELD       = 'searchField';
    const FIELD_SEARCH_TYPE        = 'searchType';
    const FIELD_SEARCH_RANGE_START = 'searchRangeStart';
    const FIELD_SEARCH_RANGE_END   = 'searchRangeEnd';
    const FIELD_SEARCH_STRICT      = 'searchStrict';

    // order by
    const FIELD_ORDER_FIELD = 'orderField';

    /**
     * Fetch params
     */

    // paginate the response
    const FETCH_PAGINATE = 0x1;

    // disable searching by request params
    const FETCH_NO_SEARCH = 0x10;

    // download the results
    const FETCH_DOWNLOAD = 0x100;

    /**
     * Get fetch params for no action
     */
    public static function nothing()
    {
        // no paginate is implied
        return FetchParams::FETCH_NO_SEARCH;
    }
}
