<?php

namespace Api\Core\Definitions;

class AppConstants
{
    // key to bind current users access token in ioc if logged in
    const IOC_ACCESS_TOKEN_KEY = 'current.user.token';

    // current request is a web request and expects a web response
    const IOC_WEB_REQUEST = 'ewity.pos.web.request';

    // date formats
    const DATE_TIME_SHORT = 'd-M-Y H:i:s';
    const DATE_SHORT      = 'd-M-Y';

    // epsilon for double comparision
    const EPSILON = 0.01;

    // transaction retry limit
    const TX_RETRY = 3;
}
