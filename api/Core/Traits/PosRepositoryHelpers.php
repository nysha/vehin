<?php

namespace Api\Core\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


trait PosRepositoryHelpers
{
    /**
     * Get for current domain
     *
     * @param bool  $applyRequestQuerying
     * @param array $columns
     *
     * @return Collection
     */
    public function getForDomain($applyRequestQuerying = true, $columns = ['*'])
    {
        $table = $this->model->getTable();

        $query = $this->query()
            ->where("$table.domain_id", $this->getCurrentDomain()->id);

        return $this->processAndGet($query, $applyRequestQuerying, $columns);
    }

    /**
     * Get an item by id for a current domain
     *
     * @param mixed $id
     *
     * @return Model|null
     */
    public function getByIdForDomain($id)
    {
        $table = $this->model->getTable();

        return $this->query()
            ->where('id', $id)
            ->where("$table.domain_id", $this->getCurrentDomain()->id)
            ->first();
    }

    /**
     * Get an item by id for a current domain or fail
     *
     * @param         $id
     *
     * @return Model|mixed
     */
    public function getByIdOrFailForDomain($id)
    {
        $obj = $this->getByIdForDomain($id);

        if ($obj === null) {
            $this->throwNotFound();

            return null;
        }

        return $obj;
    }

}
