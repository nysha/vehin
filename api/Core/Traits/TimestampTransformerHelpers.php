<?php

namespace Api\Core\Traits;


use Api\Core\Helpers\Helpers;

trait TimestampTransformerHelpers
{
    protected function getCreated($obj)
    {
        $tz = Helpers::tz();

        return [
            'at' => $obj->created_at->copy()->tz($tz)->toDateTimeString() ?? null,
            'by' => $obj->createdBy->name ?? null,
        ];
    }

    protected function getUpdated($obj)
    {
        $tz = Helpers::tz();

        return [
            'at' => $obj->updated_at->copy()->tz($tz)->toDateTimeString() ?? null,
            'by' => $obj->updatedBy->name ?? null,
        ];
    }

    protected function getDeleted($obj)
    {
        $tz = Helpers::tz();

        return [
            'at' => $obj->deleted_at->copy()->tz($tz)->toDateTimeString() ?? null,
            'by' => $obj->deletedBy->name ?? null,
        ];
    }
}
