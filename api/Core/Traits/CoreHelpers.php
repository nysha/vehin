<?php

namespace Api\Core\Traits;

use Api\Core\Definitions\AppConstants;
use Api\Exceptions\ClientException;
use Api\Exceptions\ForbiddenException;
use Api\Exceptions\InvariantViolationException;
use Api\Modules\Location\Models\Location;
use Api\Modules\User\Definitions\PermissionDefinitions;
use Api\Modules\User\Models\AccessToken;
use Api\Modules\User\Models\Domain;
use Api\Modules\User\Models\User;
use Gate;
use Illuminate\Support\Collection;


trait CoreHelpers
{
    /**
     * Get current logged in user or null
     *
     * @return User|null|mixed
     */
    protected function getLoggedInUserNullable(): ?User
    {
        return \Auth::user();
    }

    /**
     * Get the current logged in user
     *
     * @return User
     */
    protected function getLoggedInUser(): User
    {
        $user = $this->getLoggedInUserNullable();

        if ($user === null) {
            throw new InvariantViolationException("No logged in user bound");
        }

        return $user;
    }

    /**
     * Ensure current user has the domain permission
     *
     * @param string $permission
     */
    protected function ensureDomainPermission(string $permission)
    {
        $user = $this->getLoggedInUser();

        if (! $user->hasDomainPermission($permission)) {
            throw new ForbiddenException("Insufficient permissions. Permission $permission is required for Domain", [
                'permission'  => $permission,
                'authorities' => $user->getAuthorities(),
            ]);
        }
    }

    /**
     * Ensure object is owned by current domain
     *
     * @param array $objects
     */
    protected function ensureCurrentDomain(...$objects)
    {
        $domainId = $this->getCurrentDomain()->id;

        foreach ($objects as $object) {
            if ($object === null) {
                continue;
            }

            if (! isset($object->domain_id) || empty($object->domain_id)) {
                throw new InvariantViolationException("Object does not have a domain_id field");
            }

            if ($object->domain_id !== $domainId) {
                $objectName = class_basename($object) . '/' . ($object->id ?? 'null');

                throw new ClientException("Object $objectName does not belong to current domain");
            }
        }
    }

    /**
     * Ensure current user has the permission for the location
     *
     * @param Location|Collection $location
     * @param string              $permission
     */
    protected function ensureLocationPermission($location, string $permission)
    {
        if (! ($location instanceof \Traversable)) {
            $location = [$location];
        }

        foreach ($location as $obj) {
            if ($this->getLoggedInUser()->hasLocationPermission($obj, $permission)) {
                return;
            }
        }

        // fail
        $names = [];
        $ids   = [];

        foreach ($location as $obj) {
            $names[] = $obj->name;
            $ids[]   = $obj->id;
        }

        $namesJoined = implode(', ', $names);

        throw new ForbiddenException("Insufficient Permission. Permission $permission is required for locations $namesJoined", [
            'location'    => $names,
            'location_id' => $ids,
            'permission'  => $permission,
        ]);
    }

    /**
     * Ensure current user is a super admin
     *
     * @throws ForbiddenException
     */
    protected function ensureSuperAdmin()
    {
        $this->ensureDomainPermission(PermissionDefinitions::P_SUPER_ADMIN);
    }

    /**
     * Get the domain of the current logged in user
     *
     * @return Domain
     */
    protected function getCurrentDomain(): Domain
    {
        $user = $this->getLoggedInUser();

        return $user->domain;
    }

    /**
     * Get current access token
     */
    protected function getAccessToken(): AccessToken
    {
        if (! app()->bound(AppConstants::IOC_ACCESS_TOKEN_KEY)) {
            throw new InvariantViolationException("No access token bound");
        }

        $token = app(AppConstants::IOC_ACCESS_TOKEN_KEY);
        if (! $token instanceof AccessToken) {
            // no user is logged in
            throw new InvariantViolationException("AccessToken instance bound is of class " . class_basename($token));
        }

        return $token;
    }

    /**
     * Is a user logged in
     *
     * @return bool
     */
    protected function isUserLoggedIn()
    {
        return $this->getLoggedInUserNullable() != null;
    }

    /**
     * Ensure the user can view the given object. Uses
     * auth policies to validate
     *
     * @param mixed $obj any object
     *
     */
    protected function gateEnsureRead($obj)
    {
        Gate::authorize('read', $obj);
    }

    /**
     * Ensure the user can delete the given object. Uses
     * auth policies to validate
     *
     * @param $obj
     */
    protected function gateEnsureDelete($obj)
    {
        Gate::authorize('delete', $obj);
    }

    /**
     * Ensure the object can be updated
     *
     * @param $obj
     */
    protected function gateEnsureUpdate($obj)
    {
        Gate::authorize('update', $obj);
    }
}
