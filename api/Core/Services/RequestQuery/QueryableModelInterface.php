<?php

namespace Api\Core\Services\RequestQuery;

interface QueryableModelInterface
{
    /**
     * Get default ordering to apply
     *
     * @return array
     */
    public static function getDefaultOrdering();

    /**
     * Get default pagination limit
     *
     * @return int
     */
    public static function getDefaultPaginationLimit();

    /**
     * Search config for model
     *
     * @return array|null
     */
    public static function getSearchConfig();

    /**
     * Get order config
     *
     * @return array|null
     */
    public static function getOrderConfig();

    /**
     * Get config for joining for searches and ordering
     *
     * @return array
     */
    public static function getJoinConfig();
}
