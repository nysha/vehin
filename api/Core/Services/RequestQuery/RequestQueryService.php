<?php

namespace Api\Core\Services\RequestQuery;

use Api\Core\Helpers\Helpers;
use Api\Exceptions\ClientException;
use Api\Exceptions\InvariantViolationException;
use Api\Exceptions\ServerException;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


/**
 * Class RequestQueryService
 *
 * Helps to achieve pagination, querying, ordering and other features
 * when querying using eloquent based on parameters passed in the request
 *
 * @package Api\Core\Services
 */
class RequestQueryService
{
    /**
     * Boolean value, if evaluates to true result set will be downloaded
     */
    const FIELD_DOWNLOAD = 'download';

    /**
     * Row limit for download
     */
    protected $downloadRowLimit;

    /**
     * Boolean value, if evaluates to true result set will not be paginated
     */
    const FIELD_NO_PAGINATE = 'no_paginate';

    /**
     * Page number
     */
    const FIELD_PAGE = 'page';

    /**
     * Limit (number of items per page) to be used
     */
    const FIELD_LIMIT = 'limit';

    /**
     * String, field to order by. Can specify direction with a colon
     *
     * Eg: id      => will order by id field
     *     id:desc => will order by id field desc
     */
    const FIELD_ORDER_FIELD = 'order_by';

    /**
     * @var \Request
     */
    private $request;

    /**
     * RequestQueryService constructor.
     *
     * @param Request $request
     */
    public function __construct(
        Request $request
    ) {
        $this->request          = $request;
        $this->downloadRowLimit = config('vehin.request-querying.max');
    }

    /**
     * @param Builder $query
     * @param array   $columns
     *
     * @return LengthAwarePaginator|Collection
     */
    public function apply($query, $columns = ['*'])
    {
        /** @var QueryableModel|Model $model */
        $model = $query->getModel();

        // for querying to work the model must use the trait
        if (! $model instanceof QueryableModelInterface) {
            throw new InvariantViolationException(class_basename($model) . ' does not implement QueryableModelInterface');
        }

        // select table fields, this is required to not mess up things when doing joins
        $query->select([$model->getTable() . '.*']);

        // configs to apply
        $ordering = $this->getOrdering($model);
        $searches = $this->getSearches($model);

        $this->performRequiredJoins($model, $query, $ordering, $searches);

        // apply search
        $this->applySearch($model, $query, $searches);

        // apply ordering
        $query->orderBy($ordering['_field'] ?? $ordering['key'], $ordering['direction']);

        return $this->applyPaginationAndGet($query, $model, $columns);
    }

    /**
     * @param QueryableModelInterface|Model $model
     * @param Builder                       $query
     * @param                               $ordering
     * @param                               $searches
     */
    protected function performRequiredJoins($model, $query, $ordering, $searches)
    {
        $searchConfig = $model::getSearchConfig()['searches'];

        $joins = [];

        // order joins
        $joins[] = $ordering['_join'] ?? null;

        // search joins
        foreach ($searches as $key => $value) {
            if (isset($searchConfig[$key]['_join'])) {
                $joins[] = $searchConfig[$key]['_join'];
            }
        }

        $joins = array_values(array_unique(array_filter(array_flatten($joins))));

        $joinConfig = $model::getJoinConfig();

        // perform joins
        foreach ($joins as $join) {
            if (! isset($joinConfig[$join])) {
                throw new ServerException("Join config is not specified for $join");
            }

            $conf = $joinConfig[$join];

            if ($conf instanceof \Closure) {
                $conf($query);

            } else {
                if (count($conf) !== 4) {
                    throw new ServerException("Join config is expected to have 4 parts");
                }

                $query->join($conf[0], $conf[1], $conf[2], $conf[3]);
            }
        }
    }

    /**
     * Apply searches
     *
     * @param QueryableModelInterface|Model $model
     * @param Builder                       $query
     * @param                               $searches
     */
    protected function applySearch($model, $query, $searches)
    {
        $searchConfig = $model::getSearchConfig()['searches'];

        foreach ($searches as $key => $value) {
            $conf  = $searchConfig[$key];
            $type  = $conf['type'] ?? null;
            $field = $conf['_field'] ?? $key;

            if (isset($conf['_method'])) {
                app()->call($conf['_method'], [$query, $value]);
                continue;
            }

            if ($type === 'date-range') {
                $parts = explode(',', $value);
                if (count($parts) !== 2) {
                    throw new ClientException("For date-range query expected YYYY-MM-DD,YYYY-MM-DD");
                }

                $first  = Carbon::createFromFormat('Y-m-d', $parts[0], Helpers::tz());
                $second = Carbon::createFromFormat('Y-m-d', $parts[1], Helpers::tz());

                if (isset($conf['_time']) && $conf['_time']) {
                    $first->startOfDay();
                    $second->endOfDay();
                }

                $first->tz('UTC');
                $second->tz('UTC');

                $query->whereBetween($field, [$first, $second]);
                continue;
            }

            // default
            $op = isset($conf['_op']) ? $conf['_op'] : '=';

            // handle op in value (<500, >500, >=500, etc)
            if (isset($conf['_numeric']) && $conf['_numeric']) {
                if (strlen($value) > 2) {
                    $twoChars = substr($value, 0, 2);

                    if (in_array($twoChars, ['>=', '<='])) {
                        $op    = $twoChars;
                        $value = substr($value, 2);
                    }

                }

                if (strlen($value) > 1) {
                    $oneChar = $value[0];

                    if (in_array($oneChar, ['>', '<'])) {
                        $op    = $oneChar;
                        $value = substr($value, 1);
                    }
                }
            }

            $query->where($field, $op, ($op === 'like' ? "%${value}%" : $value));
        }
    }

    /**
     * @param Builder                       $query
     * @param QueryableModelInterface|Model $model
     * @param                               $columns
     *
     * @return LengthAwarePaginator|Collection
     */
    protected function applyPaginationAndGet($query, $model, $columns)
    {
        if (! $this->shouldPaginate()) {
            return $this->getNonPaginated($query, $columns);
        }

        $page  = $this->getPageNumber();
        $limit = $this->getPaginationLimit($model);

        return $query->paginate($limit, $columns, 'page', $page);
    }

    /**
     * Get result non paginated
     *
     * @param Builder $query
     * @param array   $columns
     *
     * @return Model[]|Collection
     */
    protected function getNonPaginated($query, $columns)
    {
        // run a count
        $rowCount = $query->count();
        if ($rowCount > $this->downloadRowLimit) {
            throw new ClientException("Your filters returned $rowCount rows. Please restrict your filters and try again");
        }

        return $query->get($columns);
    }

    /**
     * Get the searches to apply
     *
     * @param QueryableModelInterface|Model $model
     *
     * @return array
     */
    protected function getSearches($model)
    {
        $config = $model::getSearchConfig()['searches'];

        $searchFields = collect($this->request->all())
            ->filter(function ($v, $k) {
                return Str::startsWith($k, 'q_');
            })
            ->toArray();

        // validate all fields
        $out = [];
        foreach ($searchFields as $field => $value) {
            // remove q_
            $field = substr($field, 2);

            if (! isset($config[$field])) {
                throw new ClientException("Invalid search field $field");
            }

            if ($value === '' || $value === null) {
                continue;
            }

            $out[$field] = $value;
        }

        return $out;
    }

    /**
     * Get the ordering to apply
     *
     * @param QueryableModelInterface|Model $model
     *
     * @return array
     * @throws ClientException
     */
    protected function getOrdering($model)
    {
        $orderString = $this->request->input(static::FIELD_ORDER_FIELD);

        if (empty($orderString)) {
            // get default ordering from model
            list($key, $direction) = $model::getDefaultOrdering();

            return ['key' => $key, 'direction' => $direction];
        }

        $parts = explode(':', $orderString);
        if (count($parts) > 2) {
            throw new ClientException("Order field is expected to be of format COL(:DIR)?");
        }

        if (count($parts) === 1) {
            // default direction
            $parts[1] = 'DESC';
        }

        if (! in_array(strtoupper($parts[1]), ['DESC', 'ASC'])) {
            throw new ClientException("Invalid order direction ${parts[1]}, expected DESC or ASC");
        }

        // validate order key
        $orderings = $model::getOrderConfig()['orderings'];
        if (! isset($orderings[$parts[0]])) {
            throw new ClientException("Invalid order field $parts[0]");
        }

        $config              = $orderings[$parts[0]];
        $config['key']       = $parts[0];
        $config['direction'] = $parts[1];

        return $config;
    }

    /**
     * Get the page number
     *
     * @return int
     */
    public function getPageNumber()
    {
        return $this->request->has(static::FIELD_PAGE) ?
            (int)$this->request->input(static::FIELD_PAGE) :
            1;
    }

    /**
     * Get the pagination limit to use
     *
     * @param QueryableModelInterface $model
     *
     * @return int
     */
    public function getPaginationLimit(QueryableModelInterface $model)
    {
        return $this->request->has(static::FIELD_LIMIT) ?
            (int)$this->request->input(static::FIELD_LIMIT) :
            $model->getDefaultPaginationLimit();
    }

    /**
     * Pagination defaults to true. Download requests will not be paginated
     *
     * @return bool
     */
    public function shouldPaginate()
    {
        if ($this->shouldDownload()) {
            return false;
        }

        return ! ((bool)$this->request->input(static::FIELD_NO_PAGINATE, false));
    }

    /**
     * Should the result set be downloaded
     *
     * @return bool
     */
    public function shouldDownload()
    {
        return (bool)$this->request->input(static::FIELD_DOWNLOAD, false);
    }

    /**
     * Download row limit
     *
     * @return int
     */
    public function getDownloadRowLimit()
    {
        return $this->downloadRowLimit;
    }

    /**
     * Get ordering from request
     *
     * @return array|null
     * @throws ClientException
     */
    public function getRequestOrdering()
    {
        $orderString = $this->request->input(static::FIELD_ORDER_FIELD);
        if (empty($orderString)) {
            return null;
        }

        $parts = explode(':', $orderString);
        if (count($parts) > 2) {
            throw new ClientException("Order field is expected to be of format COL(:DIR)?");
        }

        if (count($parts) === 1) {
            // default direction
            $parts[1] = 'DESC';
        }

        if (! in_array(strtoupper($parts[1]), ['DESC', 'ASC'])) {
            throw new ClientException("Invalid order direction ${parts[1]}, expected DESC or ASC");
        }

        return [$parts[0], $parts[1]];
    }
}
