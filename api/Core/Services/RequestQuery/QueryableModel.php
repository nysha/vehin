<?php

namespace Api\Core\Services\RequestQuery;

use Api\Exceptions\ServerException;


trait QueryableModel
{
    /**
     * Get default ordering to apply
     *
     * @return array
     */
    public static function getDefaultOrdering()
    {
        $ordering = static::$orderConfig;

        if (empty($ordering) || ! isset($ordering['default'])) {
            return null;
        }

        return $ordering['default'];
    }

    /**
     * Get default pagination limit
     *
     * @return int
     */
    public static function getDefaultPaginationLimit()
    {
        return static::$defaultPaginationLimit;
    }

    /**
     * Search config for model
     *
     * @return array|null
     */
    public static function getSearchConfig()
    {
        $config = static::$searchConfig;
        if (!isset($config['searches'])) {
            throw new ServerException("Invalid search config. searches key missing");
        }

        return $config;
    }

    /**
     * Get order config
     *
     * @return array|null
     */
    public static function getOrderConfig()
    {
        $config = static::$orderConfig;
        if (!isset($config['orderings'])) {
            throw new ServerException("Invalid order config. orderings key missing");
        }

        return $config;
    }

    /**
     * Get config for joining for searches and ordering
     *
     * @return array
     */
    public static function getJoinConfig()
    {
        return static::$joinConfig;
    }
}
