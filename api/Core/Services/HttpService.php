<?php

namespace api\Core\Services;

use GuzzleHttp;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Log;


/**
 * Class HttpService
 *
 * Helper service for making HTTP requests with Guzzle
 */
class HttpService
{
    /**
     * Should errors be logged verbosely
     *
     * @var bool
     */
    private $verboseErrorLogging = false;
    /**
     * Headers to be set on every request
     *
     * @var array
     */
    private $headers = [];

    /**
     * Make a get request
     *
     * @param string $url         url to make a request to
     * @param array  $data        array of data to send as get params
     * @param bool   $rawResponse if set the raw response will be returned
     *
     * @return array [statusCode, responseData] response data returned by server
     */
    public function get($url, $data = null, $rawResponse = false)
    {
        if ($data != null&&! empty($data)) {
            $url .= '?' . http_build_query($data);
        }

        return $this->makeRequest('GET', $url, null, $rawResponse);
    }

    /**
     * Make a post request
     *
     * @param string $url       url to make a request to
     * @param array  $data      array of data to send as json body params
     * @param array  $urlParams optional url get params
     * @param bool   $rawResponse
     * @param bool   $formData
     *
     * @return array [statusCode, responseData] response data returned by server
     */
    public function post($url, $data, $urlParams = null, $rawResponse = false, $formData = false)
    {
        if ($urlParams != null&&! empty($urlParams)) {
            $url .= '?' . http_build_query($urlParams);
        }

        return $this->makeRequest('POST', $url, $data, $rawResponse, $formData);
    }

    /**
     * Make a put request
     *
     * @param      $url
     * @param      $data
     * @param null $urlParams
     *
     * @return array
     */
    public function put($url, $data, $urlParams = null)
    {
        if ($urlParams != null&&! empty($urlParams)) {
            $url .= '?' . http_build_query($urlParams);
        }

        return $this->makeRequest('PUT', $url, $data);
    }

    /**
     * Make the HTTP request
     *
     * @param string      $method      GET or POST
     * @param string      $url         the url to make the request to
     * @param array|mixed $body        optional body data
     * @param bool        $rawResponse if set the response data will not be manipulated and a raw guzzle response
     *                                 will be returned
     * @param bool        $formData
     *
     * @return array array of [responseCode, responseObject]
     */
    private function makeRequest($method, $url, $body = null, $rawResponse = false, $formData = false)
    {
        $client = new GuzzleHttp\Client();

        $startTime = microtime(true);

        try {
            $curlOpts = [];
            if (defined('CURLOPT_IPRESOLVE')&&defined('CURL_IPRESOLVE_V4')) {
                // force ipv4 as ipv6 times out for allied auth and causes a delay
                $curlOpts[CURLOPT_IPRESOLVE] = CURL_IPRESOLVE_V4;
            }

            $requestData = [
                'headers' => $this->headers,
                'curl'    => $curlOpts,
            ];

            if ($formData) {
                $requestData['form_params'] = $body;
            } else {
                $requestData['json'] = $body;
            }

            $response   = $client->request($method, $url, $requestData);
            $statusCode = $response->getStatusCode();

            $requestTime = number_format((microtime(true) - $startTime) * 1000, 0);

            Log::info("[HTTPService] HTTP ${method} ${url} ${statusCode} time=${requestTime}ms");

            if ($rawResponse) {
                return [$statusCode, $response->getBody(), null];
            }

            // try to convert to json
            $body = GuzzleHttp\Psr7\copy_to_string($response->getBody());

            return [$statusCode, $this->json($body), $body];
        } catch (ClientException $e) {
            // 4XX HTTP errors
            $response    = $e->hasResponse() ? GuzzleHttp\Psr7\copy_to_string($e->getResponse()->getBody()) : null;
            $status      = $e->getResponse()->getStatusCode();
            $requestBody = json_encode($body);

            // we assume that the user tried to log in with a wrong code
            $errorMessage = "Client exception status=${status}";
            if ($this->verboseErrorLogging) {
                $errorMessage = "Client exception status=${status} body=${requestBody} res=${response}";
            }

            $requestTime = number_format((microtime(true) - $startTime) * 1000, 0);

            Log::info("HTTP ${method} ${url} ${status} time={$requestTime}ms");
            Log::notice($errorMessage);

            return [$status, $this->json($response), $response];
        } catch (ServerException $e) {
            // 5XX HTTP errors
            $response    = $e->hasResponse() ? GuzzleHttp\Psr7\copy_to_string($e->getResponse()->getBody()) : null;
            $message     = $e->getMessage();
            $status      = $e->getResponse()->getStatusCode();
            $requestBody = json_encode($body);

            $errorMessage = "Server exception e=${message}";
            if ($this->verboseErrorLogging) {
                $errorMessage = "Server exception status=${status} e=${message} body=${requestBody} res=${response}";
            }

            $requestTime = number_format((microtime(true) - $startTime) * 1000, 0);

            Log::info("HTTP ${method} ${url} ${status} time={$requestTime}ms");
            Log::warning($errorMessage);

            return [$status, $this->json($response), $response];
        } catch (RequestException $e) {
            // exceptions like DNS, connection error etc
            $message = $e->getMessage();

            Log::info("HTTP ${method} ${url} -1");
            Log::error("RequestException status=-1 e=${message}");

            return [-1, null, null];
        }
    }

    /**
     * Given data string which might be json returns a json encoded object,
     * if not json returns the string as is
     *
     * @param string $data  string data which might be json
     * @param bool   $assoc should json be decoded as assoc
     *
     * @return mixed|string json object or string data as is
     */
    public function json($data, $assoc = true)
    {
        if ($data == null) {
            return $data;
        }

        $jsonObject = json_decode($data, $assoc);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $jsonObject;
        } else {
            return null;
        }
    }

    /**
     * Should errors be logged
     *
     * @param $errorLogging boolean should error logging be enabled
     */
    public function setVerboseErrorLogging($errorLogging)
    {
        $this->verboseErrorLogging = $errorLogging;
    }

    /**
     * Clears all headers
     */
    public function clearHeaders()
    {
        $this->headers = [];
    }

    /**
     * Add an header
     *
     * @param $key   string key
     * @param $value string value of header
     */
    public function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
    }
}
