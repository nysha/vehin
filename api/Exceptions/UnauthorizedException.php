<?php

namespace Api\Exceptions;

/**
 * Class UnauthorizedException
 *
 * Thrown when the credentials given are invalid or expired
 *
 * @package Api\Exceptions
 */
class UnauthorizedException extends ClientException
{
    protected $httpCode = 401;

    public function __construct($data, $debug = null)
    {
        parent::__construct($data, $debug, ExceptionCodes::UNAUTHORIZED);
    }
}
