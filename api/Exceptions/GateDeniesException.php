<?php

namespace Api\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;


class GateDeniesException extends AuthorizationException
{
    protected $debug;

    /**
     * GateDeniesException constructor.
     *
     * @param string|array $perms
     * @param null         $arg
     * @param null         $reason
     */
    public function __construct($perms, $arg = null, $reason = null)
    {
        parent::__construct(ExceptionCodes::getExceptionMessage(ExceptionCodes::FORBIDDEN));

        $this->debug = [
            'type'                => 'GateDeniesException',
            'requestedPermission' => $perms,
            'reason'              => $reason,
            'arg'                 => $arg,
        ];

        if ($reason != null) {
            $this->message = $reason;
        }
    }

    public function getDebug()
    {
        return $this->debug;
    }
}
