<?php

namespace Api\Exceptions;

class ValidationException extends ClientException
{
    protected $httpCode = 422;

    /**
     * ValidationException constructor.
     *
     * @param array|string $errors
     * @param null         $debugData
     */
    public function __construct($errors, $debugData = null)
    {
        if (is_array($errors) == false && is_string($errors)) {
            $errors = ["form" => [$errors]];
        }

        parent::__construct($errors, $debugData, ExceptionCodes::VALIDATION_ERROR);

        // set message
        if (! empty($errors)) {
            parent::setMessage('There are errors in ' . implode(', ', array_keys($errors)));
        }
    }
}
