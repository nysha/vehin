<?php

namespace Api\Exceptions;

class ConfigurationException extends ServerException
{
    /**
     * ConfigurationException constructor.
     *
     * @param array|string $message
     * @param array|string $debug
     */
    public function __construct($message, $debug = null)
    {
        parent::__construct($message, $debug, ExceptionCodes::CONFIGURATION_ERROR);
    }
}
