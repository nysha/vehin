<?php

namespace Api\Modules\Device\Services;

use Api\Modules\Device\Repositories\DeviceRepository;
/**
 * Class DeviceService
 *
 * @package \Api\Modules\Device\Services
 */
class DeviceService
{
    /**
     * @var DeviceRepository
     */
    protected $repository;

    /**
     * DeviceService constructor.
     *
     * @param DeviceRepository $repository
     */
    public function __construct(DeviceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new Device record
     *
     * @param array $inputs
     *
     * @return Device
     */
    public function store($inputs)
    {
        return $this->repository->store($inputs);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function currentVehicle($id)
    {
        return $this->repository->currentVehicle($id);
    }

    /**
     * all devices
     *
     * @return mixed
     */
    public function getDevices()
    {
        return $this->repository->allDevices();
    }

    /**
     * All active devices
     *
     * @return \Api\Modules\Device\Repositories\Collection
     */
    public function getAllActiveDevices()
    {
        return $this->repository->allActiveDevices();
    }
}
