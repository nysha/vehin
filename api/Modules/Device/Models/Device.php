<?php

namespace Api\Modules\Device\Models;

use Api\Core\Base\BaseModel;
use Api\Modules\VehicleDevice\Models\VehicleDevice;

/**
 * Class device
 *
 * @package \Api\Modules\Device\Models
 */
class Device extends BaseModel
{
    const DEVICE_TYPE_PHONE = 'phone';

    protected $fillable = [
        'active',
        'lat',
        'lng',
        'mobile_no',
        'type',
        'brand'
    ];

    public function vehicle() {
        return $this->belongsTo(VehicleDevice::class);
    }
}
