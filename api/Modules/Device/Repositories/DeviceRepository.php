<?php

namespace Api\Modules\Device\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\Device\Models\Device;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceRepository
 *
 * @package \Api\Modules\Device\Repositories
 */
class DeviceRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Device::class;
    }

    /**
     * @param $inputs
     *
     * @return Device
     */
    public function store($inputs)
    {
        return $this->create($inputs);
    }

    /**
     * @param $id
     *
     * @return mixedq
     */
    public function currentVehicle($id)
    {
        return $this->model->vehicle()->whereNull('end_date')->first();
    }

    /**
     * Get all devices
     *
     * @return mixed
     */
    public function allDevices() {
        return $this->model->all();
    }

    /**
     * Get all active devices
     *
     * @return Collection
     */
    public function allActiveDevices() {
        return $this->model->where('active', "=", 1)->get();
    }

    /**
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data)
    {
        return parent::update($model, $data);
    }


}
