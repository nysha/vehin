<?php

namespace Api\Modules\Device\Transformers;

use Azaan\LaravelScene\SceneTransformer;
/**
 * Class DeviceTransformer
 *
 * @package \Api\Modules\Device\Transformers
 */
class DeviceTransformer extends SceneTransformer
{
    /**
     * Minimal structure
     *
     * @return array
     */
    public function getMinStructure()
    {
        $zoneTransformer  = new ZoneTransformer();
        return [
            'id',
            'active',
            'latitude' => 'lat',
            'longitude' => 'lng'
        ];
    }

    /**
     * Structure transformations
     *
     * @return array
     */
    public function getStructure()
    {
        $zoneTransformer  = new ZoneTransformer();
        return [
            'id',
            'active',
            'latitude' => 'lat',
            'longitude' => 'lng'
        ];
    }
}
