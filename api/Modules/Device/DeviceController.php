<?php

namespace Api\Modules\Device;

use Api\Core\Base\BaseController;
use Api\Modules\Device\Services\DeviceService;
use Api\Modules\Device\Transformers\DeviceTransformer;

/**
 * Class DeviceController
 *
 * @package \Api\Modules\Device
 */
class DeviceController extends BaseController
{
    /**
     * @var DeviceService
     */
    private $deviceService;

    /**
     * DeviceController constructor.
     *
     * @param DeviceService $service
     */
    public function __construct(DeviceService $service)
    {
        $this->deviceService = $service;
    }

    /**
     * Get all devices
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function all()
    {
        $data = $this->deviceService->getDevices();

        $transformer = new DeviceTransformer();

        return $this->respond($data, $transformer);
    }

    /**
     * All active devices
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function allActiveDevices()
    {
        $data = $this->deviceService->getAllActiveDevices();

        $transformer = new DeviceTransformer();

        return $this->respond($data, $transformer);
    }
}
