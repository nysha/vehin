<?php

namespace Api\Modules\BusRoute\Transformers;

use Api\Modules\BusRoute\Models\Route;
use Api\Modules\Zone\Transformers\ZoneTransformer;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class UserTransformer
 *
 * @package \Api\Modules\User\Transformers
 */
class BusRouteTransformer extends SceneTransformer
{

    /**
     * Minimal structure
     *
     * @return array
     */
    public function getMinStructure()
    {
        $zoneTransformer  = new ZoneTransformer();
        return [
            'id',
            'Route Name' => 'route_name',
            'Route Desc' => 'desc',
            'Zone' =>  $zoneTransformer,
            'created_at'
        ];
    }

    /**
     * Structure transformations
     *
     * @return array
     */
    public function getStructure()
    {
        $zoneTransformer  = new ZoneTransformer();
        return [
            'id',
            'Route Name' => 'route_name',
            'Route Desc' => 'desc',
            'Zone' =>  $zoneTransformer,
            'created_at'
        ];
    }

    protected function getZone(Route $zone)
    {
        return $zone->zone;
    }




}
