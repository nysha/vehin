<?php

namespace Api\Modules\BusRoute;

use Api\Core\Base\BaseController;
use Illuminate\Http\JsonResponse;
use Api\Modules\BusRoute\Services\BusRouteService;
use Api\Modules\BusRoute\Transformers\BusRouteTransformer;
use Illuminate\Http\Request;

/**
 * Class BusRouteController
 *
 * @package \Api\Modules\Zones
 */
class BusRouteController extends BaseController
{
    /**
     * @var \Api\Modules\Zones\Services\UserService
     */
    private $busRouteService;

    /**
     * BusRouteController constructor.
     *
     * @param BusRouteService $busRouteService
     */
    public function __construct(BusRouteService $busRouteService)
    {
       $this->busRouteService = $busRouteService;
    }



   public function all()
    {
  
        $data = $this->busRouteService->getRoutes();
       
        $transformer = new BusRouteTransformer();

        return $this->respond($data, $transformer);

    }

    public function store(Request $request)
    {
    

        $data = $this->busRouteService->createRoute($request->all());
       
        $transformer = new BusRouteTransformer();

        return $this->respond($data, $transformer);

    }

    public function delete(Request $request)
    {
        

        $data = $this->busRouteService->deleteBusRoute($request->get('id'));
    

        return $this->respond("BusRoute Has been Deleted ".$request->get('id'), null);

    }

    public function update()
    {
  
        $data = $this->busRouteService->updateZone();
       
        $transformer = new ZoneTransformer();

        return $this->respond($data, $transformer);

    }

}
