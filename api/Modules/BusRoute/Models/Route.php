<?php

namespace Api\Modules\BusRoute\Models;
use Api\Core\Base\BaseModel;
use Api\Modules\Stop\Models\Stop;
use Api\Modules\Zone\Models\Zone;


/**
 * Class Islands
 *
 * @property integer                                  $id
 * @property string                                   $
 * @property string                                   $name
 * @property string                                   $zones
 * @property string                                   $created_by
 * @property datetime                                 $updated_at
 */
class Route extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];

//    protected $rememberTokenName = '';

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

    public function stops()
    {
        return $this->belongsToMany(Stop::class, 'routes_stops')
            ->withPivot(['d_stop_id', 'o_dep_time', 'd_arr_time', 'order'])
            ->withTimestamps();
    }
   
}
