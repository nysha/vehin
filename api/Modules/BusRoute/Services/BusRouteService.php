<?php

namespace Api\Modules\BusRoute\Services;

use Api\Exceptions\ClientException;
use Api\Modules\BusRoute\Repositories\BusRouteRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class IslandService
 *
 * @package \Api\Modules\Island\Services
 */
class BusRouteService
{
    protected $repository;

    /**
     * IslandService constructor.
     *
     * @param IslandRepository $repository
     */
    public function __construct(BusRouteRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new user record
     *
     * @param array $inputs
     *
     * @return \Api\Modules\User\Models\User
     */
    public function getRoutes()
    {

        return $this->repository->getAllRoutes();
    }

    public function createRoute($inputs)
    {
        return $this->repository->createBusRoute($inputs);
    }

    public function deleteBusRoute($inputs)
    {
        return $this->repository->deleteRoutes($inputs);
    }

    
   
}
