<?php

namespace Api\Modules\BusRoute\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\BusRoute\Models\Route;

/**
 * Class BusRouteRepository
 *
 * @package \Api\Modules\User\Repositories
 */
class BusRouteRepository extends BaseRepository
{
    /**
     * Get model class name
     *
     * @return string
     */
    public function model()
    {
        return Route::class;
    }

    /**
     * get all BusRoute record
     *
     * @return BusRoute
     *
     */
    public function getAllRoutes()
    {
        return $this->model->all();
    }

    public function createBusRoute($inputs)
    {
        return $this->model->create($inputs);
    }

    public function deleteRoutes($inputs)
    {
        return $this->model->destroy($inputs);
    }

}
