<?php

namespace Api\Modules\Vehicle\Services;
use Api\Modules\Vehicle\Repositories\VehicleRepository;
/**
 * Class VehicleService
 *
 * @package \Api\Modules\Vehicle\Services
 */
class VehicleService
{
    /**
     * @var VehicleRepository
     */
    protected $repository;

    /**
     * VehicleService constructor.
     *
     * @param VehicleRepository $repository
     */
    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new Vehicle record
     *
     * @param array $inputs
     *
     * @return Vehicle
     */
    public function store($inputs)
    {
        return $this->repository->store($inputs);
    }

    /**
     * @return mixed
     */
    public function getAllVehicles()
    {
        return $this->repository->getAllVehicles();
    }
}
