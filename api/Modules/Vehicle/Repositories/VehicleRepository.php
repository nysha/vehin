<?php

namespace Api\Modules\Vehicle\Repositories;
use Api\Core\Base\BaseRepository;
use Api\Modules\Vehicle\Models\Vehicle;

/**
 * Class VehicleRepository
 *
 * @package \Api\Modules\Vehicle\Repositories
 */
class VehicleRepository extends BaseRepository
{
    public function model()
    {
        return Vehicle::class;
    }

    /**
     * @param $inputs
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store($inputs)
    {
        return $this->create($inputs);
    }

    /**
     * Get all vehicles
     *
     * @return mixed
     */
    public function getAllVehicles() {
        return $this->model->all();
    }
}
