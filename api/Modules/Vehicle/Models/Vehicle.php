<?php

namespace Api\Modules\Vehicle\Models;
use Api\Core\Base\BaseModel;
use Api\Modules\VehicleDevice\Models\VehicleDevice;

/**
 * Class Vehicle
 *
 * @package \Api\Modules\Vehicle\Models
 */
class Vehicle extends BaseModel
{
    const VEHICLE_TYPE_BUS = 'bus';
    const VEHICLE_TYPE_FERRY = 'ferry';
    const VEHICLE_TYPE_PRIVATE_HIRE = 'private_hire';

    protected $guarded = [
        'device_id'
    ];

    public function devices() {
        return $this->belongsTo(VehicleDevice::class);
    }
}
