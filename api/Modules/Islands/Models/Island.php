<?php

namespace Api\Modules\Islands\Models;
use Api\Core\Base\BaseModel;



/**
 * Class Islands
 *
 * @property integer                                  $id
 * @property string                                   $
 * @property string                                   $name
 * @property string                                   $zones
 * @property string                                   $created_by
 * @property datetime                                 $updated_at
 */
class Island extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $rememberTokenName = '';


   
}
