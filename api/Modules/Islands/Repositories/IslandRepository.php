<?php

namespace Api\Modules\Islands\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\Islands\Models\Island;

/**
 * Class UserRepository
 *
 * @package \Api\Modules\User\Repositories
 */
class IslandRepository extends BaseRepository
{
    /**
     * Get model class name
     *
     * @return string
     */
    public function model()
    {
        return Island::class;
    }

    /**
     * Create a new user record
     *
     * @param array $inputs
     *
     * @return User
     *
     */
    public function getAllIslands()
    {
        return $this->model->all();
    }

}
