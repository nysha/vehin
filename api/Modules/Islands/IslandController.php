<?php

namespace Api\Modules\Islands;

use Api\Core\Base\BaseController;
use Illuminate\Http\JsonResponse;
use Api\Modules\Islands\Services\IslandService;
use Api\Modules\Islands\Transformers\IslandTransformer;
/**
 * Class IslandController
 *
 * @package \Api\Modules\Islands
 */
class IslandController extends BaseController
{
    /**
     * @var \Api\Modules\Islands\Services\UserService
     */
    private $islandService;

    /**
     * IslandController constructor.
     *
     * @param \Api\Modules\Islands\Services\UserService $islandService
     */
    public function __construct(IslandService $islandService)
    {
       $this->islandService = $islandService;
    }



   public function all()
    {
  
         $users = $this->islandService->getIslands();
       
        $transformer = new IslandTransformer();

        return $this->respond($users, $transformer);

    }

}
