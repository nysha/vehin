<?php

namespace Api\Modules\Islands\Services;

use Api\Exceptions\ClientException;
use Api\Modules\Islands\Repositories\IslandRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class IslandService
 *
 * @package \Api\Modules\Island\Services
 */
class IslandService
{
    protected $repository;

    /**
     * IslandService constructor.
     *
     * @param IslandRepository $repository
     */
    public function __construct(IslandRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new user record
     *
     * @param array $inputs
     *
     * @return \Api\Modules\User\Models\User
     */
    public function getIslands()
    {

        return $this->repository->getAllIslands();
    }

    // /**
    //  * Find a user by id
    //  *
    //  * @param int $id
    //  * @return User
    //  */
    // public function findOrFail($id)
    // {
    //     return $this->repository->findOrFail($id);
    // }

   
}
