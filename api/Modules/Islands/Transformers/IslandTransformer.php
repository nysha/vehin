<?php

namespace Api\Modules\Islands\Transformers;

use Api\Modules\Islands\Models\Island;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class UserTransformer
 *
 * @package \Api\Modules\User\Transformers
 */
class IslandTransformer extends SceneTransformer
{

    /**
     * Minimal structure
     *
     * @return array
     */
    public function getMinStructure()
    {
        return [
            'id',
            'island_name',
            'zone_id',
            'created_at'
        ];
    }

    /**
     * Structure transformations
     *
     * @return array
     */
    public function getStructure()
    {
        return [
            'id',
            'island_name',
            'zone_id',
            'created_at'
        ];
    }


}
