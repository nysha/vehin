<?php

namespace Api\Modules\MTCC;

use Api\Core\Base\BaseModel;

/**
 * Class MtccWebToken
 *
 * @package \Api\Modules\MTCC
 */
class MtccWebToken extends BaseModel
{
    protected $guarded = [];
}
