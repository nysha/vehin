<?php

namespace Api\Modules\Zone\Transformers;

use Api\Modules\Auth\Models\Permission;
use Api\Modules\Auth\Transformers\PermissionTransformer;
use Api\Modules\Booking\Transformers\BookingTransformer;
use Api\Modules\Islands\Models\Zone;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class UserTransformer
 *
 * @package \Api\Modules\User\Transformers
 */
class ZoneTransformer extends SceneTransformer
{

    /**
     * Minimal structure
     *
     * @return array
     */
    public function getMinStructure()
    {
        return [
            'id',
            'zone Name' => 'zone_name',
            'Zone Desc' => 'desc',
            'created_at'
        ];
    }

    /**
     * Structure transformations
     *
     * @return array
     */
    public function getStructure()
    {
        return [
            'id',
            'zone Name' => 'zone_name',
            'Zone Desc' => 'desc',
            'created_at'
        ];
    }

    protected function getZone(Zone $zone)
    {
        return $Zone->branch;
    }


}
