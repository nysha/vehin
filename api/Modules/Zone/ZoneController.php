<?php

namespace Api\Modules\Zone;

use Api\Core\Base\BaseController;
use Illuminate\Http\JsonResponse;
use Api\Modules\Zone\Services\ZoneService;
use Api\Modules\Zone\Transformers\ZoneTransformer;
use Illuminate\Http\Request;

/**
 * Class ZoneController
 *
 * @package \Api\Modules\Zones
 */
class ZoneController extends BaseController
{
    /**
     * @var \Api\Modules\Zones\Services\UserService
     */
    private $zoneService;

    /**
     * ZoneController constructor.
     *
     * @param \Api\Modules\Zones\Services\UserService $ZoneService
     */
    public function __construct(ZoneService $zoneService)
    {
       $this->zoneService = $zoneService;
    }



   public function all()
    {
  
        $data = $this->zoneService->getZone();
       
        $transformer = new ZoneTransformer();

        return $this->respond($data, $transformer);

    }

    public function store(Request $request)
    {
    

        $data = $this->zoneService->createZone($request->all());
       
        $transformer = new ZoneTransformer();

        return $this->respond($data, $transformer);

    }

    public function delete(Request $request)
    {
        

        $data = $this->zoneService->deleteZone($request->get('id'));
    

        return $this->respond("Zone Has been Deleted ".$request->get('id'), null);

    }

    public function update()
    {
  
        $data = $this->zoneService->updateZone();
       
        $transformer = new ZoneTransformer();

        return $this->respond($data, $transformer);

    }

}
