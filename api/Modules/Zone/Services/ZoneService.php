<?php

namespace Api\Modules\Zone\Services;

use Api\Exceptions\ClientException;
use Api\Modules\Zone\Repositories\ZoneRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class IslandService
 *
 * @package \Api\Modules\Island\Services
 */
class ZoneService
{
    protected $repository;

    /**
     * IslandService constructor.
     *
     * @param IslandRepository $repository
     */
    public function __construct(ZoneRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new user record
     *
     * @param array $inputs
     *
     * @return \Api\Modules\User\Models\User
     */
    public function getZone()
    {

        return $this->repository->getAllZones();
    }

    public function createZone($inputs)
    {
        return $this->repository->createZone($inputs);
    }

    public function deleteZone($inputs)
    {
        return $this->repository->deleteZone($inputs);
    }

    
   
}
