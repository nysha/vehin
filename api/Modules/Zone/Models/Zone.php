<?php

namespace Api\Modules\Zone\Models;
use Api\Core\Base\BaseModel;
use Api\Modules\BusRoute\Models\Route;


/**
 * Class Islands
 *
 * @property integer                                  $id
 * @property string                                   $
 * @property string                                   $name
 * @property string                                   $zones
 * @property string                                   $created_by
 * @property datetime                                 $updated_at
 */
class Zone extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $rememberTokenName = '';



    public function routes()
    {
        return $this->hasMany(Route::class);
    }
   
}
