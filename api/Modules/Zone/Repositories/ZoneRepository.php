<?php

namespace Api\Modules\Zone\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\Zone\Models\Zone;

/**
 * Class UserRepository
 *
 * @package \Api\Modules\User\Repositories
 */
class ZoneRepository extends BaseRepository
{
    /**
     * Get model class name
     *
     * @return string
     */
    public function model()
    {
        return Zone::class;
    }

    /**
     * get all zone record
     *
     * @return zone
     *
     */
    public function getAllZones()
    {
        return $this->model->all();
    }

    public function createZone($inputs)
    {
        return $this->model->create($inputs);
    }

    public function deleteZone($inputs)
    {
        return $this->model->destroy($inputs);
    }

}
