<?php

namespace Api\Modules\User\Models;

use Api\Modules\Auth\Models\Permission;
use Api\Modules\Booking\Models\Booking;
use Api\Modules\User\Definitions\UserDefinitions;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticable;

/**
 * Class User
 *
 * @property integer                                  $id
 * @property string                                   $employee_id
 * @property string                                   $name
 * @property string                                   $designation
 * @property string('super_admin', 'nurse', 'doctor') $type
 *
 * @property string                                   $password
 *
 * @property datetime                                 $created_at
 * @property string                                   $created_by
 * @property datetime                                 $updated_at
 * @property string                                   $updated_by
 *
 * @property-read Collection                          $permissions
 *
 * @package \Api\Modules\User\Models
 */
class User extends Authenticable
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $rememberTokenName = '';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The "booting" method of the model
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('hide_super_admin', function (Builder $builder) {
            $builder->where('name', '!=', UserDefinitions::SUPER_ADMIN_NAME);
        });
    }

    /**
     * For debugging purposes
     *
     * @return string
     */
    public function debugName()
    {
        return $this->name;
    }

    /**
     * @return BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'user_permissions');
    }

}
