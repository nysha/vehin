<?php

namespace Api\Modules\User\Transformers;

use Api\Modules\Auth\Models\Permission;
use Api\Modules\Auth\Transformers\PermissionTransformer;
use Api\Modules\Booking\Transformers\BookingTransformer;
use Api\Modules\User\Models\User;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class UserTransformer
 *
 * @package \Api\Modules\User\Transformers
 */
class UserTransformer extends SceneTransformer
{

    /**
     * Minimal structure
     *
     * @return array
     */
    public function getMinStructure()
    {
        return [
            'id',
            'name',
            'employee_id',
            'type'
        ];
    }

    /**
     * Structure transformations
     *
     * @return array
     */
    public function getStructure()
    {
        return [
            'id',
            'name',
            'employee_id',
            'type',
            'permissions'
        ];
    }

    protected function getPermissions(User $user)
    {
        return $user->permissions->map(function(Permission $permission) {
            return $permission->slug;
        })->toArray();
    }
}
