<?php

namespace Api\Modules\User\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\User\Models\User;

/**
 * Class UserRepository
 *
 * @package \Api\Modules\User\Repositories
 */
class UserRepository extends BaseRepository
{
    /**
     * Get model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Create a new user record
     *
     * @param array $inputs
     *
     * @return User
     *
     */
    public function store()
    {
        return $this->model->all();
    }

    public function getUserByEmployeeId($empId)
    {
        $user = $this->model->withoutGlobalScopes()->where('employee_id', $empId)->first();

        return $user;
    }
}
