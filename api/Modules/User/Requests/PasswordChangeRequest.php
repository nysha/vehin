<?php

namespace Api\Modules\User\Requests;

use Api\Core\Base\BaseRequest;

/**
 * Class PasswordChangeRequest
 *
 * @package \Api\Modules\User\Requests
 */
class PasswordChangeRequest extends BaseRequest
{
    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'new_password' => 'required|confirmed'
        ];
    }

    /**
     * Custom messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'current_password.required' => "Please enter your current password!",
            "new_password.required" => "Please enter a new password",
            "new_password.confirmed" => "The new password and the confirmation password does not match!",
        ];
    }
}
