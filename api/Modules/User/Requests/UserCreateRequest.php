<?php

namespace Api\Modules\User\Requests;

use Api\Core\Base\BaseRequest;
use Api\Modules\User\Definitions\UserDefinitions;

class UserCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userTypes = implode(',', UserDefinitions::getUserTypes());
        return [
            'name' => 'required',
            'employee_id' => 'required|unique:users',
            'type' => 'in:'. $userTypes
        ];
    }
}
