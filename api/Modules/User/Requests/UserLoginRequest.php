<?php

namespace Api\Modules\User\Requests;

use Api\Core\Base\BaseRequest;
use Api\Modules\User\Definitions\UserDefinitions;

class UserLoginRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required|exists:users,employee_id',
            'password' => 'required'
        ];
    }
}
