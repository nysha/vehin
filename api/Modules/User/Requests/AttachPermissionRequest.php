<?php

namespace Api\Modules\User\Requests;
use Api\Core\Base\BaseRequest;

/**
 * Class AttachPermissionRequest
 *
 * @package \Api\Modules\User\Requests
 */
class AttachPermissionRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'permission_id' => 'required|exists:permissions,id'
        ];
    }
}
