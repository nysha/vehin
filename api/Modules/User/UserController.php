<?php

namespace Api\Modules\User;

use Api\Core\Base\BaseController;
use Api\Modules\Auth\Definitions\PermissionDefinitions;
use Api\Modules\Auth\Services\PermissionService;
use Api\Modules\Booking\Transformers\BookingTransformer;
use Api\Modules\User\Definitions\UserDefinitions;
use Api\Modules\User\Models\User;
use Api\Modules\User\Requests\AttachPermissionRequest;
use Api\Modules\User\Requests\PasswordChangeRequest;
use Api\Modules\User\Requests\UserCreateRequest;
use Api\Modules\User\Services\UserService;
use Api\Modules\User\Transformers\UserTransformer;
use Illuminate\Http\JsonResponse;
use Api\Modules\User\Requests\UserLoginRequest;


/**
 * Class UserController
 *
 * @package \Api\Modules\User
 */
class UserController extends BaseController
{
    /**
     * @var \Api\Modules\User\Services\UserService
     */
    private $userService;

    /**
     * @var PermissionService
     */
    private $permissionService;

    /**
     * UserController constructor.
     *
     * @param \Api\Modules\User\Services\UserService $userService
     * @param PermissionService                      $permissionService
     */
    public function __construct(UserService $userService, PermissionService $permissionService)
    {
        $this->userService = $userService;
        $this->permissionService = $permissionService;
    }

    /**
     * Get all user types
     */
    public function getUserTypes()
    {
        $userTypes = UserDefinitions::getUserTypes();

        return $this->respond($userTypes);
    }


    /**
     * Get logged in user
     *
     * @return JsonResponse
     */
    public function getMe()
    {
        dd("dad");
        $user = $this->getLoggedInUser();

        return $this->respond($user, new UserTransformer());
    }

    public function all()
    {
        $users = User::query()->orderBy('created_at', 'desc')->get();

        $transformer = new UserTransformer();

        return $this->respond($users, $transformer);

    }

    /**
     * Creates a user
     *
     * @param UserCreateRequest $request
     *
     * @return JsonResponse
     */
    public function store(UserCreateRequest $request)
    {
        $this->checkPermission(PermissionDefinitions::CREATE_USER);
        $user = $this->userService->store($request->validAll());

        // attach permissions assoicated with user type
        $permissions = PermissionDefinitions::getPermissionsByUserType($user->type);
        $permissions = $this->permissionService->getBySlugsOrFail($permissions);
        $user->permissions()->sync($permissions);

        return $this->respond($user, new UserTransformer());
    }


    /**
     * Change logged in user password
     *
     * @param PasswordChangeRequest $request
     *
     * @return JsonResponse;
     */
    public function passwordChange(PasswordChangeRequest $request)
    {
        $user = $this->getLoggedInUser();

        $this->userService->passwordChange($user, $request->validAll($except = ['new_password_confirmation']));

        return $this->respondOk();
    }

    /**
     * Reset a given user's passowrd
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function passwordReset($id)
    {
        $user = $this->userService->findOrFail($id);

        $user->password = $this->userService->passwordReset($user);

        return $this->respondOk();
    }

    /**
     * Attach Permissions to a User
     *
     * @param                         $id
     * @param AttachPermissionRequest $request
     *
     * @return JsonResponse|\Illuminate\Http\Response
     */
    public function attachPermission($id, AttachPermissionRequest $request)
    {
        $user = $this->userService->findOrFail($id);
        $user = $this->userService->attachPermission($user, $request->only('permission_id'));
        return $this->respond($user, new UserTransformer());

    }

    /**
     * Get Logged In User's bookings
     *
     * @return JsonResponse|\Illuminate\Http\Response
     * @throws \Api\Exceptions\InvariantViolationException
     */
    public function myBookings()
    {
        $user = $this->getLoggedInUser();

        $bookings = $this->userService->myBookings($user);

        $transformer = new UserTransformer();
        return $this->respond($user, $transformer, ['bookings' => $bookings]);
    }

    public function userLogin(UserLoginRequest $request)
    {
        $user = $this->userService->userLogin($request->all());

        $transformer = new UserTransformer();

        return $this->respond($user, $transformer);
    }

    public function userLogout()
    {
        $this->userService->userLogout();
        return $this->respond('User Logged out successfully');
    }
}
