<?php

namespace Api\Modules\User\Services;

use Api\Exceptions\ClientException;
use Api\Modules\User\Definitions\UserDefinitions;
use Api\Modules\User\Models\User;
use Api\Modules\User\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UserService
 *
 * @package \Api\Modules\User\Services
 */
class UserService
{
    protected $repository;

    /**
     * UserService constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates a new user record
     *
     * @param array $inputs
     *
     * @return \Api\Modules\User\Models\User
     */
    public function store($inputs)
    {
        $inputs['password'] = Hash::make(UserDefinitions::DEFAULT_PASSWORD);

        return $this->repository->store($inputs);
    }

    /**
     * Find a user by id
     *
     * @param int $id
     * @return User
     */
    public function findOrFail($id)
    {
        return $this->repository->findOrFail($id);
    }

    /**
     * Change user password
     *
     * @param User $user
     * @param array $inputs
     * @throws ClientException
     */
    public function passwordChange(User $user, $inputs)
    {
        if(!\Hash::check($inputs['current_password'], $user->password)) {
            throw new ClientException("The current password provided is not valid");
        }

        $user->password = \Hash::make($inputs['new_password']);
        $user->save();
    }

    /**
     * Reset user password
     *
     * @param User $user
     */
    public function passwordReset(User $user)
    {
        $user->password = Hash::make(UserDefinitions::DEFAULT_PASSWORD);
        $user->save();
    }

    /**
     * Attach permission to user
     *
     * @param User $user
     * @param int $permissionId
     *
     * @return bool
     */
    public function attachPermission($user, $permissionId)
    {
        $exists = $user->whereHas('permissions', function($query) use($permissionId) {
            $query->where('permission_id', $permissionId);
        })->count();

        if($exists > 0) {
            return true;
        }

        $user->permissions()->attach($permissionId);
        $user->save();
        return true;
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function myBookings(User $user)
    {
        return $user->bookings;
    }

    /**
     * Function to check login credentials
     * 
     * @param  array $inputs    user provides employee_id and password
     * @return User             User MOdel
     */
    public function userLogin($inputs)
    {
        $user = $this->repository->getUserByEmployeeId($inputs['employee_id']);
        if (!\Hash::check($inputs['password'], $user->password))
        {
            throw new ClientException("Invalid Employee Id or Password");
        }

        \Auth::login($user);

        return $user;
    }

    public function userLogout()
    {
        \Auth::logout();
    }
}
