<?php

namespace Api\Modules\User\Definitions;

/**
 * Class SuperAdminDefinitions
 *
 * @package \Api\Modules\Auth\Definitions
 */
class UserDefinitions
{
    // default passwords for all users
    const DEFAULT_PASSWORD = 'Welcome123$';

    const SUPER_ADMIN_NAME = 'super_admin';
    // password is secret
    const SUPER_ADMIN_PASSWORD = '$2y$10$./JquJMbRHjCfX9RmOaKruyb3jq6wJKKxINKgCWDMyNETI1xkwxmi';

    const SUPER_ADMIN_TYPE = 'super_admin';

    const USER_TYPE_ADMIN = 'admin';
    const USER_TYPE_SUPER_ADMIN = 'super_admin';

    /**
     * All user types
     *
     * @return array
     */
    public static function getUserTypes()
    {
        return [
            self::USER_TYPE_ADMIN,
            self::USER_TYPE_SUPER_ADMIN,
        ];
    }

}
