<?php

namespace Api\Modules\Stop\Models;
use Api\Core\Base\BaseModel;
use Api\Modules\BusRoute\Models\Route;

// use Api\Modules\BusRoute\Models\Route;


/**
 * Class Islands
 *
 * @property integer                                  $id
 * @property string                                   $
 * @property string                                   $name
 * @property string                                   $Stops
 * @property string                                   $created_by
 * @property datetime                                 $updated_at
 */
class Stop extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $rememberTokenName = '';

    public function routes()
    {
        return $this->belongsToMany(Route::class, 'routes_stops')
            ->withPivot(['d_stop_id', 'o_dep_time', 'd_arr_time', 'order'])
            ->withTimestamps();
    }
   
}
