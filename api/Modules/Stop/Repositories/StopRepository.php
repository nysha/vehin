<?php

namespace Api\Modules\Stop\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\Stop\Models\Stop;

/**
 * Class UserRepository
 *
 * @package \Api\Modules\User\Repositories
 */
class StopRepository extends BaseRepository
{
    /**
     * Get model class name
     *
     * @return string
     */
    public function model()
    {
        return Stop::class;
    }

    /**
     * get all zone record
     *
     * @return zone
     *
     */
    public function getAllStop()
    {
        return $this->model->all();
    }

    public function createStop($inputs)
    {
        return $this->model->create($inputs);
    }

    public function deleteStop($inputs)
    {
        return $this->model->destroy($inputs);
    }

}
