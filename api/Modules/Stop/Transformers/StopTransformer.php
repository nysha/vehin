<?php

namespace Api\Modules\Stop\Transformers;

use Api\Modules\Islands\Models\Stop;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class UserTransformer
 *
 * @package \Api\Modules\User\Transformers
 */
class StopTransformer extends SceneTransformer
{

    /**
     * Minimal structure
     *
     * @return array
     */
    public function getStructure()
    {
        return [
            'id',
            'Stop Name' => 'stop_code',
            'latitude' => 'lat',
            'longitude' => 'lng',
            'Landmark' => 'landmark',
            'created_at'
        ];
    }



    // protected function getZone(Zone $zone)
    // {
    //     return $Zone->branch;
    // }


}
