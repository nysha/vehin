<?php

namespace Api\Modules\Stop\Services;

use Api\Exceptions\ClientException;
use Api\Modules\Stop\Repositories\StopRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class IslandService
 *
 * @package \Api\Modules\Island\Services
 */
class StopService
{
    protected $repository;

    /**
     * StopService constructor.
     *
     * @param StopRepository $repository
     */
    public function __construct(StopRepository $repository)
    {
        $this->repository = $repository;
    }


    public function getStops()
    {

        return $this->repository->getAllStop();
    }

    public function createstop($inputs)
    {
        return $this->repository->createStop($inputs);
    }

    public function deleteStop($inputs)
    {
        return $this->repository->deleteStop($inputs);
    }

    
   
}
