<?php

namespace Api\Modules\Stop;

use Api\Core\Base\BaseController;
use Illuminate\Http\JsonResponse;
use Api\Modules\Stop\Services\StopService;
use Api\Modules\Stop\Transformers\StopTransformer;
use Illuminate\Http\Request;

/**
 * Class StopController
 *
 * @package \Api\Modules\Stops
 */
class StopController extends BaseController
{
    /**
     * @var \Api\Modules\Stops\Services\UserService
     */
    private $stopService;

    /**
     * stopController constructor.
     *
     * @param \Api\Modules\stops\Services\UserService $stopService
     */
    public function __construct(StopService $stopService)
    {
       $this->stopService = $stopService;
    }



   public function all()
    {
  
        $data = $this->stopService->getStops();
       
        $transformer = new StopTransformer();

        return $this->respond($data, $transformer);

    }

    public function store(Request $request)
    {
    

        $data = $this->stopService->createstop($request->all());
       
        $transformer = new StopTransformer();

        return $this->respond($data, $transformer);

    }

    public function delete(Request $request)
    {
        

        $data = $this->stopService->deleteStop($request->get('id'));
    

        return $this->respond("Zone Has been Deleted ".$request->get('id'), null);

    }

    public function update()
    {
  
        $data = $this->stopService->updateStop();
       
        $transformer = new StopTransformer();

        return $this->respond($data, $transformer);

    }

}
