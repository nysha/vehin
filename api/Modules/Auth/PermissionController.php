<?php

namespace Api\Modules\Auth;

use Api\Core\Base\BaseController;
use Api\Modules\Auth\Requests\PermissionCreateRequest;
use Api\Modules\Auth\Services\PermissionService;
use Api\Modules\Auth\Transformers\PermissionTransformer;
use Illuminate\Http\Request;

/**
 * Class PermissionController
 *
 * @package \Api\Modules\Auth
 */
class PermissionController extends BaseController
{
    /**
     * @var PermissionService
     */
    private $permissionService;

    /**
     * PermissionController constructor.
     *
     * @param PermissionService $permissionService
     */
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $permissions = $this->permissionService->list();
        return $this->respond($permissions);
    }

    /**
     * @param PermissionCreateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PermissionCreateRequest $request)
    {
        $permission = $this->permissionService->store($request->pData());



        $transformer = new PermissionTransformer();

        return $this->respond($permission, $transformer);
    }




}
