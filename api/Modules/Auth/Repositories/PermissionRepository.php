<?php

namespace Api\Modules\Auth\Repositories;

use Api\Core\Base\BaseRepository;
use Api\Modules\Auth\Models\Permission;

/**
 * Class PermissionRepository
 *
 * @package \Api\Modules\Auth\Repositories
 */
class PermissionRepository extends BaseRepository
{
    /**
     * Get the model the repository is for
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    public function store($inputs)
    {
        return $this->model->create($inputs);
    }
}
