<?php

namespace Api\Modules\Auth\Transformers;
use Azaan\LaravelScene\SceneTransformer;

/**
 * Class PermissionTransformer
 *
 * @package \Api\Modules\Auth\Transformers
 */
class PermissionTransformer extends SceneTransformer
{
    /**
     * @return array
     */
    public function getStructure()
    {
        return [
            'id',
            'slug',
            'description'
        ];
    }

    public function getMinStructure()
    {
        return [
            'slug'
        ];
    }
}
