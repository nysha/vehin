<?php

namespace Api\Modules\Auth\Models;

use Api\Core\Base\BaseModel;
use Api\Modules\User\Models\User;

/**
 * Class Permission
 *
 * @property integer         $id
 * @property string          $name
 * @property string          $slug
 * @property string          $text
 *
 * @property datetime        $created_at
 * @property datetime        $updated_at
 *
 * @property-read Collection $users
 *
 * @package \Api\Modules\Permission
 */
class Permission extends BaseModel
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
