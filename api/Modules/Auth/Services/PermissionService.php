<?php

namespace Api\Modules\Auth\Services;

use Carbon\Carbon;
use Api\Modules\Auth\Models\Permission;
use Api\Modules\Auth\Definitions\PermissionDefinitions;
use Api\Modules\Auth\Repositories\PermissionRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PermissionService
 *
 * @package \Api\Modules\Auth\Services
 */
class PermissionService
{
    /**
     * @var PermissionRepository
     */
    private $repository;

    /**
     * PermissionService constructor.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->repository = $permissionRepository;
    }

    /**
     * Get all permissions
     *
     * @return Collection
     */
    public function all()
    {
        return $this->repository->all();
    }

    /**
     * Creates a new permission record
     *
     * @param array $inputs
     *
     * @return Permission
     */
    public function store($inputs)
    {
        return $this->repository->store($inputs);
    }

    /**
     * Get permissions by slugs
     *
     * @param array $slugs
     *
     * @return Collection
     */
    public function getBySlugsOrFail($slugs)
    {
        return $this->repository->getByKeysOrFail($slugs, 'slug');
    }

    /**
     * @param $slug
     *
     * @return Permission
     */
    public function findBySlugOrFail($slug)
    {
        return $this->repository->findByOrFail('slug', $slug);
    }

    public function syncPermissionsTable()
    {
        try {
            $permissions = PermissionDefinitions::get();

            foreach ($permissions as $slug => $permission) {
                if (is_null(Permission::where('slug', $slug)->get()->first())) {
                    // add permission to table
                    Permission::insert(
                        [
                            'name' => $permission['name'],
                            'slug' => $slug,
                            'description' => $permission['description'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]
                    );
                }
            }
            return true;
        } catch (\Exception $e) {
            throw new ServerException('An error occured while upating permissions: ' . $e);
        }
    }

}
