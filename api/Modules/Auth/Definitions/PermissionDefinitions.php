<?php

namespace Api\Modules\Auth\Definitions;

use Api\Modules\User\Definitions\UserDefinitions;

/**
 * Class PermissionDefinitions
 *
 * @package \Api\Modules\Auth\Definitions
 */
class PermissionDefinitions
{

    const CREATE_STOP = 'create-stop';
    const UPDATE_SCHEDULE = 'update-schedule';

    /**
     * Get permissions by user type
     *
     * @param string $type
     *
     * @return array
     */
    public static function getPermissionsByUserType($type)
    {
        $roles = self::getUserTypePermissions();
        return $roles[$type];
    }

    /**
     * Get all user type permissions
     *
     * @return array
     */
    public static function getUserTypePermissions()
    {
        $allPermissions = array_keys(self::get());

        return [
            // super_admin permissions
            UserDefinitions::USER_TYPE_SUPER_ADMIN => $allPermissions,

            // admin permissions
            UserDefinitions::USER_TYPE_ADMIN => [
                self::CREATE_STOP,
            ],
        ];
    }

    /**
     * Get all permissions
     *
     * @return array
     */
    public static function get()
    {
        return [
            self::CREATE_STOP => [
                'name' => 'create stop',
                'slug' => SELF::CREATE_STOP,
                'description' => 'Create new Stop'
            ],
            self::UPDATE_SCHEDULE => [
                'name' => 'update schedule',
                'slug' => SELF::UPDATE_SCHEDULE,
                'description' => 'Update Schedule'
            ],
        ];
    }
}
