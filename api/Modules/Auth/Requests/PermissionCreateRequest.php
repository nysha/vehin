<?php

namespace Api\Modules\Auth\Requests;
use Api\Core\Base\BaseRequest;

/**
 * Class PermissionCreateRequest
 *
 * @package \Api\Modules\Auth\Requests
 */
class PermissionCreateRequest extends BaseRequest
{
    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'string',
        ];
    }

    /**
     * Processed data
     *
     * @return array
     */
    public function pData()
    {
        $inputs = $this->validAll();
        $inputs['slug'] = str_slug($inputs['name']);

        return $inputs;
    }

}
