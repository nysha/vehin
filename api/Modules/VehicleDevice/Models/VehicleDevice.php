<?php

namespace Api\Modules\VehicleDevice\Models;

use Api\Core\Base\BaseModel;
use Api\Modules\Device\Models\Device;
use Api\Modules\Vehicle\Models\Vehicle;

/**
 * Class VehicleDevice
 *
 * @package \Api\Modules\VehicleDevice\Models
 */
class VehicleDevice extends BaseModel
{
    protected $guarded = [];

    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }

    public function devcies() {
        return $this->hasMany(Device::class);
    }
}
