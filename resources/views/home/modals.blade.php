<div class="modals">
  <!-- Modal -->

  <!-- Modal for Bus Stop / START -->
  <div class="modal fade right" id="exampleModalPreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header d-flex">
          <h5 class="modal-title mr-auto" id="exampleModalPreviewLabel">Henveyru Track</h5>
          <li class="list-inline-item pr-2 black-text mr-auto"><i class="fas fa-walking"></i> 5 mins
          </li>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <!-- Card -->
              <div class="card mb-4">
                <!-- Card content -->
                <div class="card-body">

                  <div class="d-flex">
                    <!-- Title -->
                    <h4 class="card-title mr-auto">Next Buses</h4>
                    <!-- <h5><span class="badge badge-default mr-auto">G44234</span></h5> -->
                  </div>
                  <hr>
                  <!-- Text -->
                  <div class="d-flex">
                    <div class="card-text mr-auto">Male' - Hulhumale</div>
                    <li class="list-inline-item pr-2 black-text"><i class="fas fa-2x fa-bus"></i>
                    </li>

                    <!-- <div class="list-inline-item pr-2 black-text ml-auto"><img src="img/Icons/bus-stop.png" alt="" height="20px" width="20px"> 5 mins</div> -->
                  </div>
                </div>

                <!-- Card footer -->
                <div class="rounded-bottom mdb-color lighten-3 text-center pt-3 d">
                  <!-- continue from here -->
                  <ul class="list-unstyled list-inline font-small">
                    <li class="list-inline-item pr-2 white-text mr-4"><i class="fas fa-clock"></i> Every 25 mins</li>
                    <li class="list-inline-item pr-2 black-text ml-4"><i class="fas fa-wifi"></i> ETA 5 mins</li>
                  </ul>
                </div>

              </div>
              <!-- Card -->
            </div>
            <div class="col-md-12">
              <!-- Card -->
              <div class="card mb-4">
                <!-- Card content -->
                <div class="card-body">

                  <div class="d-flex">
                    <!-- Title -->
                    <h4 class="card-title mr-auto">Next Buses</h4>
                    <!-- <h5><span class="badge badge-default mr-auto">G44234</span></h5> -->
                  </div>
                  <hr>
                  <!-- Text -->
                  <div class="d-flex">
                    <div class="card-text mr-auto">Male' - Hulhumale</div>
                    <li class="list-inline-item pr-2 black-text">
                      <i class="fas fa-2x fa-bus"></i>
                    </li>

                    <!-- <div class="list-inline-item pr-2 black-text ml-auto"><img src="img/Icons/bus-stop.png" alt="" height="20px" width="20px"> 5 mins</div> -->
                  </div>
                </div>

                <!-- Card footer -->
                <div class="rounded-bottom mdb-color lighten-3 text-center pt-3 d">
                  <!-- continue from here -->
                  <ul class="list-unstyled list-inline font-small">
                    <li class="list-inline-item pr-2 white-text mr-4"><i class="fas fa-clock"></i> Every 25 mins</li>
                    <li class="list-inline-item pr-2 black-text ml-4"><i class="fas fa-wifi"></i> ETA 5 mins</li>
                  </ul>
                </div>

              </div>
              <!-- Card -->
            </div>
          </div> <!-- row -->
        </div>
      </div>
    </div>
  </div>
  <!-- Modal for Bus Stop / END -->

  <!--Modal: Nearby Bus Stops -->
  <div class="modal fade" id="modalBusStops" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

      <!--Content-->
      <nav class="navbar navbar-dark indigo">
        <span class="navbar-text white-text">
              Navbar text with an inline element
            </span>
        <button type="button" data-dismiss="modal" aria-label="close"><i
              class="fas fa-times"></i>
        </button>
        // Continue from here
        <button type="button" data-dismiss="modal" class="close" aria-label="Close">
          <span aria-hidden="true" class="fas fa-times fa-0.5x"></span>
        </button>
      </nav>
      <div class="modal-content">
        <!--Body-->
        <div class="modal-body mb-0 p-0">

          <!--Google map-->
          <div id="map-nearby-stops" class="z-depth-1-half map-container-11" style="height: 400px">
            <!-- <iframe src="https://maps.google.com/maps?q=los%20angeles&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe> -->
          </div>

        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: Nearby Bus Stops -->

  <!--Modal: Next Buses -->
  <div class="modal fade" id="modalNextBuses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

      <!--Content-->
      <div class="modal-content">
        <h3>Next Buses</h3>
        <!--Body-->
        <div class="modal-body mb-0 p-0">

          <!--Google map-->
          <div id="map-container-google-18" class="z-depth-1-half map-container-11" style="height: 400px">
            <iframe src="https://maps.google.com/maps?q=los%20angeles&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

        </div>

        <!--Footer-->
        <div class="modal-footer justify-content-center">
          <!-- 
              <button type="button" class="btn btn-secondary btn-md">Save location <i
                  class="fas fa-map-marker-alt ml-1"></i></button>
              <button type="button" class="btn btn-outline-secondary btn-md" data-dismiss="modal">Close <i
                  class="fas fa-times ml-1"></i></button> -->
          <!-- Vertical Steppers -->
          <div class="row mt-1">
            <div class="col-md-12">

              <!-- Stepers Wrapper -->
              <ul class="stepper stepper-vertical">

                <li class="completed">
                  <a href="#!">
                        <span class="circle"></span>
                        <span class="label">Red Wave</span>
                      </a>
                </li>

                <li class="completed">
                  <a href="#!">
                          <span class="circle"></span>
                          <span class="label">Red Wave <span class="text-muted small green-text"> - Nearest Bus Stop</span></span>
                        </a>
                </li>

                <!-- Second Step -->
                <li class="active">
                  <!--Section Title -->
                  <a href="#!">
                        <span class="circle"></span>
                        <span class="label">Million Bookshop</span>
                      </a>

                  <!-- Section Description -->
                  <div class="step-content grey lighten-3 mr-auto">
                    <p>Every 25 Mins</p>
                    <p>View Schedule</p>
                  </div>
                </li>

                <!-- Fourth Step -->
                <li>
                  <a href="#!">
                        <span class="circle"></span>
                        <span class="label">Donad</span>
                      </a>
                </li>

              </ul>
              <!-- /.Stepers Wrapper -->

            </div>
          </div>

        </div>

      </div>
      <!--/.Content-->

    </div>
  </div>
  <!--Modal: Next Buses -->

  <!-- Bus Schedules / START -->
  <div class="modal fade" id="busSchedule" tabindex="-1" role="dialog" aria-labelledby="busScheduleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="busScheduleLabel">Bus Schedule</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <!--Section: Group of personal cards-->
          <section class="mt-3">
            <!--Card group-->
            <div class="card-group">
              <!--Card-->
              <div class="card card-personal mb-4">

                <!--Card content-->
                <div class="card-body">
                  <!--Title-->
                  <a><h4 class="card-title">Central Bus </h4></a>
                  <!--Text-->
                  <hr>
                  <a class="card-meta"><span><i class="fas fa-clock"></i> 6:00am to 1:00am</span></a>
                  <p class="card-meta float-right">Every 35 mins</p>
                </div>
                <!--Card content-->

              </div>
              <!--Card-->
            </div>
            <!--Card group-->

            <!--Card group-->
            <div class="card-group">
              <!--Card-->
              <div class="card card-personal mb-4">

                <!--Card content-->
                <div class="card-body">
                  <!--Title-->
                  <a><h4 class="card-title">Airport Shuttle</h4></a>
                  <!--Text-->
                  <hr>
                  <a class="card-meta"><span><i class="fas fa-clock"></i> 6:00am to 1:00am</span></a>
                  <p class="card-meta float-right">Every 35 mins</p>
                </div>
                <!--Card content-->

              </div>
              <!--Card-->
            </div>
            <!--Card group-->

            <!--Card group-->
            <div class="card-group">
              <!--Card-->
              <div class="card card-personal mb-4">

                <!--Card content-->
                <div class="card-body">
                  <!--Title-->
                  <a><h4 class="card-title">Hulhumale' Ferry</h4></a>
                  <!--Text-->
                  <hr>
                  <a class="card-meta"><span><i class="fas fa-clock"></i> 6:00am to 1:00am</span></a>
                  <p class="card-meta float-right">Every 35 mins</p>
                </div>
                <!--Card content-->

              </div>
              <!--Card-->
            </div>
            <!--Card group-->


          </section>
          <!--Section: Group of personal cards-->
        </div>
      </div>
    </div>
  </div>
  <!-- Bus Schedules / END -->



  <!-- Modal -->
</div>
