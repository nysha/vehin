<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="The Bus App">
  <meta name="theme-color" content="#24BF93" />
  <title>Vehin</title>
  <link rel="manifest" href="/manifest.json">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
  <style>


  </style>
</head>

<body>

  <!-- Start your project here-->
  <header>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color">

      <!-- Navbar brand -->
      <a class="navbar-brand" href="#">Bus App</a>

      <!-- Collapse button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Collapsible content -->
      <div class="collapse navbar-collapse" id="basicExampleNav">

        <!-- Links -->
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#busSchedule">Schedule</a>
          </li>
        </ul>
        <div class="mx-auto">
          <span class="navbar-text white-text">
            Niyama
          </span>
        </div>
       
        <!-- Links -->
        <button id="btnAdd" aria-label="Install" hidden>add</button>
        <button id="butRefresh" aria-label="Refresh">Refresh</button>


        <form class="form-inline">
          <div class="md-form my-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Enter Detstination" aria-label="Enter Detstination">
          </div>
        </form>
      </div>
      <!-- Collapsible content -->

    </nav>

    <br>
    <!--/.Navbar-->
  </header>

  <main>
    <div>
      <!-- <div id="map"></div> -->
    </div>
  </main>
  <!-- /Start your project here-->

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- <div id="map"></div> -->
        <div class="card">
          <!--Google map-->
          <div id="map" class="z-depth-1-half map-container" style="height: 500px">
          </div>

          <!-- <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                 Card 
                <div class="card card-size mx-auto">
                  Card content
                  <div class="card-body">

                    <div class="d-flex">
                      Title
                      <h4 class="card-title mr-3">Nearby Stops</h4>
                      <button class="btn btn-indigo hoverable white-text no-shadow btn-sm ml-auto" id="showAll" data-toggle="modal" data-target="#modalBusStops"> Show All</button>
                    </div>
                    <hr>
                    Text
                    <div class="d-flex" data-toggle="modal" data-target="#exampleModalPreview">
                      <div class="card-text mr-auto">Henveyru Track</div>
                      <li class="list-inline-item pr-2 black-text"><i class="fas fa-2x fa-bus"></i></li>

                      <div class="list-inline-item pr-2 black-text ml-auto"><img src="img/Icons/bus-stop.png" alt="" height="20px" width="20px"> 5 mins</div>
                    </div>
                  </div>

                  Card footer
                  <div class="rounded-bottom mdb-color lighten-3 text-center pt-3">
                    <ul class="list-unstyled list-inline font-small">
                      <li class="list-inline-item pr-2 white-text"><i class="fas fa-walking"></i> 5 mins</li>
                    </ul>
                  </div>

                </div>

                <br>
                Card
              </div>
              <div class="col-md-6">
                Card
                <div class="card card-size mx-auto">
                  Card content
                  <div class="card-body" data-toggle="modal" data-target="#modalNextBuses">

                    <div class="d-flex">
                      Title
                      <h4 class="card-title mr-auto">Next Buses</h4>
                      <h5><span class="badge badge-default mr-auto">G44234</span></h5>
                    </div>
                    <hr>
                    Text
                    <div class="d-flex">
                      <div class="card-text mr-auto">Male' - Hulhumale</div>
                      <li class="list-inline-item pr-2 black-text"> <i class="fas fa-2x text teal-text fa-bus"></i></li>
                    </div>
                  </div>

                  Card footer
                  <div class="rounded-bottom mdb-color lighten-3 text-center pt-3 d">
                    continue from here
                    <ul class="list-unstyled list-inline font-small">
                      <li class="list-inline-item pr-2 white-text mr-4"><i class="fas fa-clock"></i> Every 25 mins</li>
                      <li class="list-inline-item pr-2 black-text ml-4"><i class="fas fa-wifi"></i> ETA 5 mins</li>
                    </ul>
                  </div>

                </div>

                <br>
                Card
              </div>
            </div> row

          </div> -->

        </div>

        <br>

        <!--Google Maps-->
      </div>
    </div>

    @include('home.modals')

  </div>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.3.1..min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->

  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>

  <!-- CODELAB: Add the install script here -->
  <script src="/js/install.js"></script>

  <!-- Google maps -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjs1Emgnnh7iPYtd2sQulGEYfeewHAuCY&callback=initMap">
  </script>

  <!-- Loads the entire Firebase JavaScript SDK -->
  <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js"></script>

  <script>
  // TODO: Replace the following with your app's Firebase project configuration
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCKKPkKgyZ85tDf0Qbukp4xtPObQOO1bS8",
    authDomain: "bus-tracker-a38ce.firebaseapp.com",
    databaseURL: "https://bus-tracker-a38ce.firebaseio.com",
    projectId: "bus-tracker-a38ce",
    storageBucket: "bus-tracker-a38ce.appspot.com",
    messagingSenderId: "798888127565"
  };
  firebase.initializeApp(config);

  var db = firebase.firestore();
  var map;
  var myLatLng = { // center map
    lat: 4.2168111,
    lng: 73.5450408
  };
  var markers = [];

  function initMap() { // Google Map Initialization... 
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: myLatLng,
      mapTypeId: 'terrain'
    });

    var stopLocations = [
        {lat: 4.217087, lng: 73.537262},
        {lat: 4.211018, lng: 73.5398207},
        {lat: 4.2155149, lng: 73.5365396},
        {lat: 4.2141728, lng: 73.5391124},
        {lat: 4.2139761, lng: 73.5408501},
        {lat: 4.2142314, lng: 73.5410588},
        {lat: 4.2186883, lng: 73.5384521},
        {lat: 4.2145001, lng: 73.5382342},
        {lat: 4.2146856, lng: 73.5408536},
        {lat: 4.21555992, lng: 73.54456876}
      ]

    // console.log(stopLocations);
 
    var busRoutesCoordinates = [
      {lat: 4.216647, lng: 73.537723},
      {lat: 4.212489, lng: 73.536664},
      {lat: 4.212315, lng: 73.536730},
      {lat: 4.211351, lng: 73.540279},
      {lat: 4.210877, lng: 73.541756},
      {lat: 4.210807, lng: 73.542101}
    ];

    addStopMarker(stopLocations);
    busRoutes(busRoutesCoordinates);
  }

  // load current vehicles
  locationsRef = db.collection('users').get().then(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {

      addBusMarker(doc.data());

      // create dynamic onChange functions for evrey vehicle
      db.collection("users").doc('' + doc.data().id + '').onSnapshot(function(vehicle) {
        markers[vehicle.data().id].setMap(null);
        addBusMarker(vehicle.data());
      });

    });
  }).catch(function(error) {
    console.log("Error getting document:", error);
  });

  function addBusMarker(data) {
    // console.log(data);
    var location = {
      lat: data.lat,
      lng: data.lng
    };
    var marker = new google.maps.Marker({
      // id:data.id,
      position: location,
      icon: "{{ url()->current() }}" + '/images/bus.png',
      map: map
    });
    markers[data.id] = marker; // add marker in the markers array...
    // document.getElementById("buses").innerHTML = cars_count;
  }

  function addStopMarker(stopLocations) {
    // Add some markers to the map.
    // Note: The code uses the JavaScript Array.prototype.map() method to
    // create an array of markers based on a given "locations" array.
    // The map() method here has nothing to do with the Google Maps API.
    var markers = stopLocations.map(function(location, i) {
      return new google.maps.Marker({
        position: location,
        icon: "{{ url()->current() }}" + '/images/busstop.png',
        map: map
      });
    });
  }

  function busRoutes(busRoutesCoordinates) {
    // 4.218164, 73.539549
    // 4.219002, 73.542683

    var busPath = new google.maps.Polyline({
      path: busRoutesCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    busPath.setMap(map);
  }

  </script>
  <script>
    // CODELAB: Register service worker.
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js')
            .then((reg) => {
              console.log('Service worker registered.', reg);
            }, (err) => {
              console.log('Service worker registration failed.', err);
            });
      });
    }
  </script>
  <script>;

    // This is for Bus Stops 
    // $(document).ready(function() {

    //   $("#showAll").click(function(e) {

    //   var busStopsAPI = "{{ url()->current() }}/Api/Stop/all";

    //   console.log(busStopsAPI);

    //     $.getJSON( busStopsAPI, {
    //       tags: "mount rainier",
    //       tagmode: "any",
    //       format: "json"
    //     })
    //     .done(function( data ) {
    //       console.log( data );
    //     }).fail(function() {
    //       console.log('bustop API fail');
    //     });
    //   })();
    // });
    
</script>


</body>

</html>
